package com.bigdata.flink.pipeline.join

import java.time.ZoneId

import org.apache.flink.table.api.{EnvironmentSettings, TableEnvironment}

/**
 * @descri ArrayExpansionExample001
 *
 * @author lj.michale
 * @date 2022-12-13
 */
object ArrayExpansionExample001 {

  def main(args: Array[String]): Unit = {

    // 定义Table环境
    val settings = EnvironmentSettings
      .newInstance()
      .inStreamingMode()
      .build()
    val tEnv = TableEnvironment.create(settings)

    // 指定时区
    tEnv.getConfig.setLocalTimeZone(ZoneId.of("Asia/Shanghai"))

    val showLogTableSql =
      """-- 曝光日志数据
        |CREATE TABLE show_log_table (
        |    log_id BIGINT,
        |    show_params ARRAY<STRING>
        |) WITH (
        |  'connector' = 'datagen',
        |  'rows-per-second' = '1',
        |  'fields.log_id.min' = '1',
        |  'fields.log_id.max' = '10'
        |);
        |""".stripMargin
    tEnv.executeSql(showLogTableSql)

    // 结果表
    val sinkTableSql =
      """
        |CREATE TABLE sink_table (
        |    log_id BIGINT,
        |    show_param STRING
        |) WITH (
        |  'connector' = 'print'
        |);
        |""".stripMargin
    tEnv.executeSql(sinkTableSql)

    val joinSQL =
      """
        |INSERT INTO sink_table
        |SELECT
        |    log_id,
        |    t.show_param as show_param
        |FROM show_log_table
        |-- array 炸开语法
        |CROSS JOIN UNNEST(show_params) AS t (show_param)
        |""".stripMargin
    tEnv.executeSql(joinSQL)

  }

}
