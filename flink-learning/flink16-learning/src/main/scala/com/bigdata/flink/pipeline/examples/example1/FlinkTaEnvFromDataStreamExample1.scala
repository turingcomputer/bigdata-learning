package com.bigdata.flink.pipeline.examples.example1

import org.apache.commons.lang3.time.FastDateFormat
import org.apache.flink.api.common.RuntimeExecutionMode
import org.apache.flink.api.common.eventtime._
import org.apache.flink.streaming.api.scala.{StreamExecutionEnvironment, createTypeInformation}
import org.apache.flink.table.api.bridge.scala.StreamTableEnvironment
import org.apache.flink.table.api.{DataTypes, Schema}

class RecordTimestampAssigner extends TimestampAssigner[UserClick] {
  override def extractTimestamp(element: UserClick, recordTimestamp: Long): Long = {
    element.cTime
  }
}

class PeriodWatermarkGenerator extends WatermarkGenerator[UserClick] {
  var maxTimestamp: Long = _
  val maxOutofOrderness = 5

  // 1. 根据某个特殊的event emit watermark
  override def onEvent(event: UserClick, eventTimestamp: Long, output: WatermarkOutput): Unit = {
    maxTimestamp = math.max(event.cTime, maxTimestamp)
  }

  // 2. 定期emit watermark，通过senv.getConfig.setAutoWatermarkInterval(200L)设置emit间隔时间，默认200ms
  override def onPeriodicEmit(output: WatermarkOutput): Unit = {
    output.emitWatermark(new Watermark(maxTimestamp - maxOutofOrderness - 1))
  }
}

class MyWatermarkStrategy extends WatermarkStrategy[UserClick] {
  override def createTimestampAssigner(context: TimestampAssignerSupplier.Context): TimestampAssigner[UserClick] = {
    new RecordTimestampAssigner()
  }

  override def createWatermarkGenerator(context: WatermarkGeneratorSupplier.Context): WatermarkGenerator[UserClick] = {
    new PeriodWatermarkGenerator()
  }
}

/**
 * @descri FlinkTaEnvFromDataStreamExample1
 *
 * @author lj.michale
 * @date 2023-03-03
 */
object FlinkTaEnvFromDataStreamExample1 {

  def main(args: Array[String]): Unit = {

    val fdf = FastDateFormat.getInstance("yyyy-MM-dd HH:mm:ss")
    val senv = StreamExecutionEnvironment.getExecutionEnvironment
        senv.setRuntimeMode(RuntimeExecutionMode.STREAMING)
    val tEnv = StreamTableEnvironment.create(senv)

    val datastream1 = senv.fromElements(
      UserClick("zhang_san", "./home", fdf.parse("2022-01-27 07:58:18").getTime),
      UserClick("li_si", "./cart", fdf.parse("2022-01-27 07:59:36").getTime)
    ).assignTimestampsAndWatermarks(new MyWatermarkStrategy())

    println(datastream1.dataType)
    datastream1.print()

    val table1 = tEnv.fromDataStream(
      datastream1,
      Schema.newBuilder()
        // datastream的列要么不在schema中定义, 要么就全部定义, 定义时可以改变列的类型
        .column("name", DataTypes.STRING())
        .column("url", DataTypes.STRING())
        .column("cTime", DataTypes.BIGINT())
        // 新增列
        .columnByExpression("new_cTime", "to_timestamp(from_unixtime(cast(cTime as bigint) / 1000, 'yyyy-MM-dd HH:mm:ss'))")
        .columnByMetadata("rowtime", DataTypes.TIMESTAMP_LTZ(3))
        .watermark("rowtime", "SOURCE_WATERMARK()")
        .build()
    )

    table1.printSchema()
    table1.execute().print()

    senv.execute("FlinkTaEnvFromDataStreamExample1")

  }

}
