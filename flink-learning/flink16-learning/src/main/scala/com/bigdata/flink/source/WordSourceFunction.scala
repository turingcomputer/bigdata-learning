package com.bigdata.flink.source

import org.apache.flink.streaming.api.functions.source.SourceFunction
import scala.util.Random


class WordSourceFunction extends SourceFunction[String] {

  private var is_running = true
  private val words=Array("hello", "world", "flink", "stream", "batch", "table", "sql")

  override def run(sourceContext: SourceFunction.SourceContext[String]): Unit = {
    while(is_running) {
      val index = Random.nextInt(words.size)
      sourceContext.collect(words(index))
      // 1秒
      Thread.sleep(1000)
    }
  }

  override def cancel(): Unit = {
    is_running = false
  }

}
