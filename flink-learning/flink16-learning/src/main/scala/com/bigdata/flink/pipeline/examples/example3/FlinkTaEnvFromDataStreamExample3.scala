package com.bigdata.flink.pipeline.examples.example3


import org.apache.commons.lang3.time.FastDateFormat
import org.apache.flink.api.common.RuntimeExecutionMode
import org.apache.flink.api.common.eventtime._
import org.apache.flink.api.scala.typeutils.Types
import org.apache.flink.streaming.api.scala.{StreamExecutionEnvironment, createTypeInformation}
import org.apache.flink.table.api.bridge.scala.StreamTableEnvironment
import org.apache.flink.table.api.{DataTypes, Schema}
import org.apache.flink.table.connector.ChangelogMode
import org.apache.flink.types.{Row, RowKind}

class RecordTimestampAssigner extends TimestampAssigner[Row] {
  override def extractTimestamp(element: Row, recordTimestamp: Long): Long = {
    element.getFieldAs[Long]("f2")
  }
}

class PeriodWatermarkGenerator extends WatermarkGenerator[Row] {
  var maxTimestamp: Long = _
  val maxOutofOrderness = 5
  // 1. 根据某个特殊的event emit watermark
  override def onEvent(event: Row, eventTimestamp: Long, output: WatermarkOutput): Unit = {
    maxTimestamp = math.max(event.getFieldAs[Long]("f2"), maxTimestamp)
  }
  // 2. 定期emit watermark，通过senv.getConfig.setAutoWatermarkInterval(200L)设置emit间隔时间，默认200ms
  override def onPeriodicEmit(output: WatermarkOutput): Unit = {
    output.emitWatermark(new Watermark(maxTimestamp - maxOutofOrderness - 1))
  }
}

class MyWatermarkStrategy extends WatermarkStrategy[Row] {
  override def createTimestampAssigner(context: TimestampAssignerSupplier.Context): TimestampAssigner[Row] = {
    new RecordTimestampAssigner()
  }

  override def createWatermarkGenerator(context: WatermarkGeneratorSupplier.Context): WatermarkGenerator[Row] = {
    new PeriodWatermarkGenerator()
  }
}


object FlinkTaEnvFromDataStreamExample3 {

  def main(args: Array[String]): Unit = {

    val fdf = FastDateFormat.getInstance("yyyy-MM-dd HH:mm:ss")
    val senv = StreamExecutionEnvironment.getExecutionEnvironment
    senv.setRuntimeMode(RuntimeExecutionMode.STREAMING)
    val tEnv = StreamTableEnvironment.create(senv)

    // 有RowKind.UPDATE_BEFORE和RowKind.UPDATE_AFTER, 则为retract形式的changelog stream
    // 有RowKind.UPDATE_AFTER, 则为upsert形式的changelog stream
    val datastream1 = senv.fromElements(
      Row.ofKind(RowKind.INSERT, "zhang_san", "./home", Long.box(fdf.parse("2022-01-27 07:58:18").getTime)),
      Row.ofKind(RowKind.INSERT, "li_si", "./cart", Long.box(fdf.parse("2022-01-27 07:59:36").getTime)),
      Row.ofKind(RowKind.UPDATE_AFTER, "li_si", "./cart2", Long.box(fdf.parse("2022-01-27 07:59:36").getTime)),
      Row.ofKind(RowKind.DELETE, "zhang_san", "./home", Long.box(fdf.parse("2022-01-27 12:59:36").getTime))
    )(Types.ROW(Types.STRING, Types.STRING, Types.LONG))
      .assignTimestampsAndWatermarks(new MyWatermarkStrategy())

    println(datastream1.dataType)
    datastream1.print()

    val table1 = tEnv.fromChangelogStream(datastream1,
      Schema.newBuilder()
        .primaryKey("f0", "f1")
        .columnByMetadata("rowtime", DataTypes.TIMESTAMP_LTZ(3))
        .build(),
      ChangelogMode.upsert()
    )
    table1.printSchema()
    table1.execute().print()

    senv.execute()

  }

}
