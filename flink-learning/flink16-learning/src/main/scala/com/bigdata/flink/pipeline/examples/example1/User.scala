package com.bigdata.flink.pipeline.examples.example1

/**
    val datetime1:java.time.Instant = java.time.Instant.ofEpochMilli(3000L)
    println(datetime1)      // 1970-01-01T00:00:03Z
    val timestamp1:Long = datetime1.toEpochMilli
    println(timestamp1)     // 3000
*/
case class User(name: String, score: Double, dataTime: java.time.Instant)
