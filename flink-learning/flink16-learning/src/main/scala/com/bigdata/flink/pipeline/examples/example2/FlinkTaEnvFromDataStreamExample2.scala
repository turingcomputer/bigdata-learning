package com.bigdata.flink.pipeline.examples.example2

import com.bigdata.flink.pipeline.examples.example1.User
import org.apache.flink.api.common.RuntimeExecutionMode
import org.apache.flink.streaming.api.scala.{DataStream, StreamExecutionEnvironment}
import org.apache.flink.table.api.bridge.scala.StreamTableEnvironment
import org.apache.flink.table.api.{DataTypes, Schema, TableDescriptor}

object FlinkTaEnvFromDataStreamExample2 {

  def main(args: Array[String]): Unit = {

    val senv = StreamExecutionEnvironment.getExecutionEnvironment
    senv.setRuntimeMode(RuntimeExecutionMode.STREAMING)
    val tEnv = StreamTableEnvironment.create(senv)

    tEnv.createTemporaryTable("default_catalog.default_database.dataSource",
      TableDescriptor.forConnector("datagen")
        .schema(Schema.newBuilder()
          .column("name", DataTypes.STRING())
          .column("score", DataTypes.DOUBLE())
          .column("dataTime", DataTypes.TIMESTAMP_LTZ(3))
          .watermark("dataTime", "dataTime - interval '10' second")
          .build())
        .option("rows-per-second", "1") // 每个slot每秒产生的数据量
        .build()
    )

    // tEnv.from(TableDescriptor)
    val dataSourceTable = tEnv.from("dataSource")
    dataSourceTable.printSchema()
    // tEnv.executeSql("select * from dataSource").print()

    // tEnv.toDataStream(dataSourceTable, classOf[User])
    // tEnv.toDataStream(dataSourceTable, DataTypes.of(classOf[User]))
    val dataSourceDatastream: DataStream[User] = tEnv.toDataStream(dataSourceTable,
      // 默认是Row<String, Double, Timestamp_LTZ>类型。使用此方式可以改变列的类型
      DataTypes.STRUCTURED(
        classOf[User],
        DataTypes.FIELD("name", DataTypes.STRING()),
        DataTypes.FIELD("score", DataTypes.DOUBLE()),
        DataTypes.FIELD("dataTime", DataTypes.TIMESTAMP_LTZ(3))
      )
    )

    print(dataSourceDatastream.dataType)
    dataSourceDatastream.executeAndCollect().foreach(println)

  }

}
