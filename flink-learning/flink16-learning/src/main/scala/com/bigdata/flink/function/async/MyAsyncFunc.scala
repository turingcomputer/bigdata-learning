package com.bigdata.flink.function.async

import org.apache.flink.streaming.api.functions.async.{ResultFuture, RichAsyncFunction}
import org.apache.flink.table.runtime.context.ExecutionContext
import org.apache.flink.util.concurrent.Executors

class MyAsyncFunc extends RichAsyncFunction[(String, String), (String, String, String, String)] {

//  lazy val client: RedisClient = RedisClient.create(RedisURI.builder()
//    .withHost("localhost") // redis host
//    .withPort(6370) // redis port
//    .withPassword("Turing@2022".toCharArray) // redis password
//    .build())
//  lazy val conn: StatefulRedisConnection[String, String] = client.connect()
//  lazy val async: RedisAsyncCommands[String, String] = conn.async()
//
//  implicit lazy val executor: ExecutionContext = ExecutionContext.fromExecutor(Executors.directExecutor())
//
//
//  override def asyncInvoke(input: (String, String), resultFuture: ResultFuture[(String, String, String, String)]): Unit = {
//    val category_name_fut: RedisFuture[String] = async.hget(s"category.${input._1}", "category_name")
//    val item_name_fut: RedisFuture[String] = async.hget(s"item.${input._2}", "item_name")
//
//    // 使用thenCombineAsync合并两个redisFuture，如果查询单个维度的话直接在redisFuture后执行thenAccept即可
//    category_name_fut.thenCombineAsync(item_name_fut,
//      (t: String, u: String) => {
//        t + "," + u  // BiFunction的返回类型需要与category_name_fut的值类型一致
//      })
//      .thenAccept(x => resultFuture.complete(Iterable((input._1, x.toString.split(",")(0), input._2, x.toString.split(",")(1)))))
//  }
  override def asyncInvoke(in: (String, String), resultFuture: ResultFuture[(String, String, String, String)]): Unit = {

  }
}