package com.bigdata.flink.pipeline.examples.example4

import org.apache.flink.api.common.RuntimeExecutionMode
import org.apache.flink.streaming.api.scala.{DataStream, StreamExecutionEnvironment}
import org.apache.flink.table.api.Expressions.$
import org.apache.flink.table.api.bridge.scala.StreamTableEnvironment
import org.apache.flink.table.api.{DataTypes, Schema, long2Literal, row, string2Literal}
import org.apache.flink.table.connector.ChangelogMode
import org.apache.flink.types.Row
import scala.collection.JavaConversions.asScalaIterator

object FlinkTaEnvFromDataStreamExample4 {

  def main(args: Array[String]): Unit = {

    val senv = StreamExecutionEnvironment.getExecutionEnvironment
    senv.setRuntimeMode(RuntimeExecutionMode.STREAMING)
    val tEnv = StreamTableEnvironment.create(senv)

    val table1 = tEnv.fromValues(
      row("zhang_san", 10, "2022-01-27 15:00:06"),
      row("li_si", 100, "2022-01-27 16:00:06"),
      row("zhang_san", 20, "2022-01-27 15:00:16"),
      row("zhang_san", 30, "2022-01-27 15:00:36"),
      row("li_si", 200, "2022-01-27 16:00:56")
    ).as("name", "amount", "dataTime")

    table1.printSchema()
    table1.execute().collect().foreach(println)

    val resultTable = table1
      .groupBy($("name"))
      .select($("name"), $("amount").sum().as("amount"))

    resultTable.printSchema()
    resultTable.execute().print()


    val resultDatastream: DataStream[Row] = tEnv.toChangelogStream(
      resultTable,
      Schema.newBuilder()
        .column("name", DataTypes.STRING().bridgedTo(classOf[String]))
        .column("amount", DataTypes.BIGINT().bridgedTo(classOf[java.lang.Long]))
        .build(),
      ChangelogMode.all()
    )
    print(resultDatastream.dataType)
    resultDatastream.print()

    resultDatastream.executeAndCollect()
      .foreach(
        row => {
          println(row.getKind)
          println(row.getFieldNames(true))
        }
      )
  }

}
