package com.bigdata.flink.pipeline

import org.apache.flink.api.common.RuntimeExecutionMode
import org.apache.flink.api.common.restartstrategy.RestartStrategies
import org.apache.flink.api.java.utils.ParameterTool
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment
import org.apache.flink.table.api.{Table, TableResult}
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment
import org.slf4j.LoggerFactory

/**
 * @descri 基于flink-connector-jdbc连接mysql
 *
 * @author lj.michale
 * @date 2022-12-13
 */
object LoginEtlKafka2KafkaExample001 {

  val logger = LoggerFactory.getLogger(this.getClass)

  def main(args: Array[String]): Unit = {

    val params = ParameterTool.fromArgs(args)
    println(" ################################# ")

    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.setParallelism(1)
    env.getConfig.setGlobalJobParameters(params)
//    env.setRestartStrategy(RestartStrategies.fixedDelayRestart(10, 3))
    env.setRuntimeMode(RuntimeExecutionMode.BATCH)

    val tableEnv = StreamTableEnvironment.create(env)

    val mysqlSourceDDL =
      """
        |CREATE TABLE MyUserTable (
        |  ts_code STRING,
        |  trade_date STRING,
        |  turnover_rate STRING,
        |  volume_ratio STRING,
        |  pe STRING,
        |  pb STRING
        |) WITH (
        |   'connector' = 'jdbc',
        |   'url' = 'jdbc:mysql://localhost:3306/bigdata?useSSL=false&useUnicode=true&characterEncoding=UTF-8&characterSetResults=UTF-8&zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC',
        |   'username' = 'root',
        |   'password' = 'Turing@2022',
        |   'table-name' = 'history',
        |   'driver' = 'com.mysql.cj.jdbc.Driver'
        |)
        |""".stripMargin

    tableEnv.executeSql(mysqlSourceDDL)
    val result:TableResult = tableEnv.executeSql("select COUNT(1) as cunts, ts_code from MyUserTable GROUP BY ts_code")
    result.print()

    env.execute("LoginEtlKafka2KafkaExample001")

  }

}
