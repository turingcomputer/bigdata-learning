package com.bigdata.flink.pipeline.examples.example1

case class UserClick(name: String, url: String, cTime: Long)