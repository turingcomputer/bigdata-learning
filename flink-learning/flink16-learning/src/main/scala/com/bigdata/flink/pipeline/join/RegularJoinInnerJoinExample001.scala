package com.bigdata.flink.pipeline.join

import java.time.ZoneId

import org.apache.flink.table.api.{EnvironmentSettings, TableEnvironment}

/**
 * @descri RegularJoinInnerJoinExample001
 *
 * @author lj.michale
 * @date 2022-12-13
 */
object RegularJoinInnerJoinExample001 {

  def main(args: Array[String]): Unit = {

    // 定义Table环境
    val settings = EnvironmentSettings
      .newInstance()
      .inStreamingMode()
      .build()
    val tEnv = TableEnvironment.create(settings)

    // 指定时区
    tEnv.getConfig.setLocalTimeZone(ZoneId.of("Asia/Shanghai"))

    // 订单表
    val UserOrderTableSql =
      """
        |CREATE TABLE user_order(
        |  order_id BIGINT,
        |  ts BIGINT,
        |  d_timestamp AS TO_TIMESTAMP_LTZ(ts, 3)
        |) WITH(
        | 'connector' = 'kafka',
        | 'topic' = 'turing-log',
        | 'properties.bootstrap.servers' = 'localhost:9092',
        | 'properties.group.id' = 'jason_flink_test',
        |  -- 为了便于演示，这里使用latest-offset，每次启动都是用最新的数据
        | 'scan.startup.mode' = 'latest-offset',
        | 'properties.auto.offset.reset' = 'latest',
        | 'format' = 'json',
        | 'json.fail-on-missing-field' = 'true',
        | 'json.ignore-parse-errors' = 'false'
        |)
        |""".stripMargin
    tEnv.executeSql(UserOrderTableSql)

    // 支付表
    val PaymentFlowTableSql =
      """
        |CREATE TABLE payment_flow(
        |  order_id BIGINT,
        |  pay_money BIGINT
        |) WITH(
        | 'connector' = 'kafka',
        | 'topic' = 'payment_flow',
        | 'properties.bootstrap.servers' = 'localhost:9092',
        | 'properties.group.id' = 'jason_flink_test',
        |  -- 为了便于演示，这里使用latest-offset，每次启动都是用最新的数据
        | 'scan.startup.mode' = 'latest-offset',
        | 'properties.auto.offset.reset' = 'latest',
        | 'format' = 'json',
        | 'json.fail-on-missing-field' = 'true',
        | 'json.ignore-parse-errors' = 'false'
        |)
        |""".stripMargin
    tEnv.executeSql(PaymentFlowTableSql)

    // 结果表
    val resTableSql =
      """
        |CREATE TABLE order_payment(
        |  order_id BIGINT,
        |  d_timestamp AS TO_TIMESTAMP_LTZ(ts, 3),
        |  pay_money BIGINT
        |) WITH(
        | 'connector' = 'kafka',
        | 'topic' = 'order_payment',
        | 'properties.bootstrap.servers' = 'localhost:9092',
        | 'properties.group.id' = 'jason_flink_test',
        |  -- 为了便于演示，这里使用latest-offset，每次启动都是用最新的数据
        | 'scan.startup.mode' = 'latest-offset',
        | 'properties.auto.offset.reset' = 'latest',
        | 'format' = 'json',
        | 'json.fail-on-missing-field' = 'true',
        | 'json.ignore-parse-errors' = 'false'
        |)
        |""".stripMargin
    tEnv.executeSql(resTableSql)

    // 关联订单表与支付表
    val joinSQL =
      """
        |INERT INTO order_payment
        |SELECT
        | uo.order_id,
        | uo.d_timestamp,
        | pf.pay_money
        |FROM user_order AS uo
        |INNER JOIN payment_flow AS pf ON uo.order_id = pf.order_id
        |""".stripMargin
    tEnv.executeSql(joinSQL)

  }

}
