package com.bigdata.flink.pipeline.join

import java.time.ZoneId

import org.apache.flink.table.api.{EnvironmentSettings, TableEnvironment}

/**
 * @descri RegularJoinInnerJoinExample002
 *
 * @author lj.michale
 * @date 2022-12-13
 */
object RegularJoinInnerJoinExample002 {

  def main(args: Array[String]): Unit = {

    // 定义Table环境
    val settings = EnvironmentSettings
      .newInstance()
      .inStreamingMode()
      .build()
    val tEnv = TableEnvironment.create(settings)

    // 指定时区
    tEnv.getConfig.setLocalTimeZone(ZoneId.of("Asia/Shanghai"))

    // 曝光日志数据表
    val showLogTableSql =
      """-- 曝光日志数据
        |CREATE TABLE show_log_table (
        |    log_id BIGINT,
        |    show_params STRING
        |) WITH (
        |  'connector' = 'datagen',
        |  'rows-per-second' = '2',
        |  'fields.show_params.length' = '1',
        |  'fields.log_id.min' = '1',
        |  'fields.log_id.max' = '100'
        |)
        |""".stripMargin
    tEnv.executeSql(showLogTableSql)

    // -- 点击日志数据表
    val clickLogTableSql =
      """
        |CREATE TABLE click_log_table (
        |  log_id BIGINT,
        |  click_params     STRING
        |)
        |WITH (
        |  'connector' = 'datagen',
        |  'rows-per-second' = '2',
        |  'fields.click_params.length' = '1',
        |  'fields.log_id.min' = '1',
        |  'fields.log_id.max' = '10'
        |)
        |""".stripMargin
    tEnv.executeSql(clickLogTableSql)

    // 结果表
    val sinkTableSql =
      """
        |CREATE TABLE sink_table (
        |    s_id BIGINT,
        |    s_params STRING,
        |    c_id BIGINT,
        |    c_params STRING
        |) WITH (
        |  'connector' = 'print'
        |)
        |""".stripMargin
    tEnv.executeSql(sinkTableSql)

    // -- 流的 INNER JOIN，条件为 log_id
    val joinSQL =
      """
        |INSERT INTO sink_table
        |SELECT
        |    show_log_table.log_id as s_id,
        |    show_log_table.show_params as s_params,
        |    click_log_table.log_id as c_id,
        |    click_log_table.click_params as c_params
        |FROM show_log_table
        |INNER JOIN click_log_table ON show_log_table.log_id = click_log_table.log_id
        |""".stripMargin
    tEnv.executeSql(joinSQL)

  }

}
