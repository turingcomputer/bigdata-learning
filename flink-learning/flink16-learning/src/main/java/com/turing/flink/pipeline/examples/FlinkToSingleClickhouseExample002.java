package com.turing.flink.pipeline.examples;

import com.turing.bean.DwdOrderBean;
import com.turing.bean.RunEnv;
import com.turing.common.utils.KafkaUtils;
import com.turing.flink.sink.clickhouse.ClickHouseJDBCOutputFormat;
import com.turing.flink.sink.clickhouse.ClickHouseJDBCSinkFunction;
import com.turing.flink.sink.clickhouse.SinkSingleClickHouse;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.restartstrategy.RestartStrategies;
import org.apache.flink.connector.jdbc.JdbcStatementBuilder;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.runtime.state.hashmap.HashMapStateBackend;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.sink.SinkFunction;
import org.apache.flink.types.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;

import static org.apache.doris.shaded.org.apache.arrow.flatbuf.SparseMatrixCompressedAxis.Row;

/**
 * @descri
 *
 * @author lj.michale
 * @date 2023-02-22
 */
@Slf4j
public class FlinkToSingleClickhouseExample002 {

    private static final Logger logger = LoggerFactory.getLogger(FlinkToSingleClickhouseExample002.class);

    public static void main(String[] args) {
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        // 开启事件时间语义
        env.enableCheckpointing(200, CheckpointingMode.EXACTLY_ONCE);
        env.getCheckpointConfig().setMinPauseBetweenCheckpoints(30000);
        env.getCheckpointConfig().setCheckpointTimeout(10000);
        env.getCheckpointConfig().setMaxConcurrentCheckpoints(1);
        // 使用 externalized checkpoints，这样 checkpoint 在作业取消后仍就会被保留(如果使用savepoint取消任务的化就不用设置此参数，开启可以防止一些失误操作，不过也要注意自己手动删除记录)
        //ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION：当作业取消时，保留作业的 checkpoint。注意，这种情况下，需要手动清除该作业保留的 checkpoint。
        //ExternalizedCheckpointCleanup.DELETE_ON_CANCELLATION：当作业取消时，删除作业的 checkpoint。仅当作业失败时，作业的 checkpoint 才会被保留。
        env.getCheckpointConfig().setExternalizedCheckpointCleanup(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
        env.setRestartStrategy(RestartStrategies.fixedDelayRestart(3, 1000));
        // 状态后端-HashMapStateBackend
        env.setStateBackend(new HashMapStateBackend());
        // 等价于MemoryStateBackend
        env.getCheckpointConfig().setCheckpointStorage("file:///E:\\company\\Turing\\bigdata\\bigdata-learning\\checkpoint");

        // flink写入clickhouse分布式表
        // 解析数据 String -> Row
        DataStreamSource<String> dataStreamSource = env.fromCollection(Arrays.asList("a|b|c", "c|d|e", "x|y|z"));
        DataStream<Row> dataStreamTransform = dataStreamSource.map((MapFunction<String, org.apache.flink.types.Row>) value -> {
            String[] fields = value.split("\\|");
            Row row = new Row(3);
            row.setField(0, fields[0]);
            row.setField(1, fields[1]);
            row.setField(2, fields[2]);
            return row;
        });

        /**
         * 写出数据到 ClickHouse
         *
         * 1. 初始化 ClickHouseJDBCOutputFormat
         * 2. 初始化 ClickHouseJDBCSinkFunction
         * 3. 增加 Sink
         */
        ClickHouseJDBCOutputFormat clickHouseJDBCOutputFormat = new ClickHouseJDBCOutputFormat(
                null,
                null,
                new String[]{"1.1.1.1:8123", "1.1.1.2:8123", "1.1.1.3:8123"},
                10000,
                1000,
                "test_db",
                "test_table_local", // 配置 local 表
                new String[]{"column_1", "column_2", "column_3"}
        );
        ClickHouseJDBCSinkFunction clickHouseJDBCSinkFunction = new ClickHouseJDBCSinkFunction(clickHouseJDBCOutputFormat);
        dataStreamTransform.addSink(clickHouseJDBCSinkFunction);
    }

}
