package com.turing.flink.function.scalar;

import org.apache.flink.table.functions.ScalarFunction;

/**
 * @descri
 * 用户定义的标量函数，可以将0、1或多个标量值，映射到新的标量值
 * 为了定义标量函数，必须在 org.apache.flink.table.functions 中扩展基类Scalar Function，并实现（一个或多个）求值（eval）方法
 * 标量函数的行为由求值方法决定，求值方法必须public公开声明并命名为 eval
 * @author lj.michale
 * @date 2023-03-01
 */
public class HashCode extends ScalarFunction {

    private int factor = 13;

    public HashCode(int factor) {
        this.factor = factor;
    }

    public int eval(String id) {
        return id.hashCode() * 13;
    }
}