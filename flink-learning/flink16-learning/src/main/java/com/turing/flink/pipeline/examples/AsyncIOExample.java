package com.turing.flink.pipeline.examples;

import com.turing.common.utils.FlinkEnvUtils;
import com.turing.flink.pipeline.async.AsyncClient;
import com.turing.flink.pipeline.examples.ProcessFunctionExample001;
import com.turing.flink.source.SimpleSource;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.AsyncDataStream;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.async.AsyncFunction;
import org.apache.flink.streaming.api.functions.async.ResultFuture;
import org.apache.flink.streaming.api.functions.async.RichAsyncFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.concurrent.TimeUnit;

/**
 * @descri Example to illustrate how to use {@link AsyncFunction}.
 *
 * @author lj.michale
 * @date 2023-02-27
 */
public class AsyncIOExample {

    /** An example of {@link AsyncFunction} using an async client to query an external service. */
    private static class SampleAsyncFunction extends RichAsyncFunction<Integer, String> {
        private static final long serialVersionUID = 1L;

        private transient AsyncClient client;

        @Override
        public void open(Configuration parameters) {
            client = new AsyncClient();
        }

        @Override
        public void asyncInvoke(final Integer input, final ResultFuture<String> resultFuture) {
            client.query(input)
                    .whenComplete(
                            (response, error) -> {
                                if (response != null) {
                                    resultFuture.complete(Collections.singletonList(response));
                                } else {
                                    resultFuture.completeExceptionally(error);
                                }
                            });
        }
    }

    private static final Logger logger = LoggerFactory.getLogger(ProcessFunctionExample001.class);
    private static final String propertiesFilePath = "E:\\company\\Turing\\bigdata\\bigdata-learning\\flink-learning\\flink16-learning\\src\\main\\resources\\application-dev.properties";

    public static void main(String[] args) throws Exception {

        final  ParameterTool parameter = ParameterTool.fromPropertiesFile(propertiesFilePath);

        final String mode;
        final long timeout;

        try {
            mode = parameter.get("waitMode", "ordered");
            timeout = parameter.getLong("timeout", 10000L);
        } catch (Exception e) {
            System.out.println(
                    "To customize example, use: AsyncIOExample [--waitMode <ordered or unordered>]");
            throw e;
        }

        // obtain execution environment
        final StreamExecutionEnvironment env = FlinkEnvUtils.getStreamExecutionEnv(parameter);
        // StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        // create input stream of a single integer
        DataStream<Integer> inputStream = env.addSource(new SimpleSource());

        AsyncFunction<Integer, String> function = new SampleAsyncFunction();

        // add async operator to streaming job
        DataStream<String> result;
        switch (mode.toUpperCase()) {
            case "ORDERED":
                result =
                        AsyncDataStream.orderedWait(
                                inputStream, function, timeout, TimeUnit.MILLISECONDS, 20);
                break;
            case "UNORDERED":
                result =
                        AsyncDataStream.unorderedWait(
                                inputStream, function, timeout, TimeUnit.MILLISECONDS, 20);
                break;
            default:
                throw new IllegalStateException("Unknown mode: " + mode);
        }

        result.print();

        // execute the program
        env.execute("Async IO Example: " + mode);
    }
}
