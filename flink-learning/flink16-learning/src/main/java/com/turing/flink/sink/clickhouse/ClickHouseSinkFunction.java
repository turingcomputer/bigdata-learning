package com.turing.flink.sink.clickhouse;

import org.apache.flink.api.common.eventtime.Watermark;
import org.apache.flink.runtime.state.FunctionInitializationContext;
import org.apache.flink.runtime.state.FunctionSnapshotContext;
import org.apache.flink.streaming.api.checkpoint.CheckpointedFunction;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;

/**
 * @descri
 *
 * @author lj.michale
 * @date 2023-02-22
 */
public class ClickHouseSinkFunction<T> extends RichSinkFunction<T> implements CheckpointedFunction {

    @Override
    public void snapshotState(FunctionSnapshotContext functionSnapshotContext) throws Exception {

    }

    @Override
    public void initializeState(FunctionInitializationContext functionInitializationContext) throws Exception {

    }

    @Override
    public void invoke(T value, Context context) throws Exception {

    }

    @Override
    public void writeWatermark(Watermark watermark) throws Exception {

    }

    @Override
    public void finish() throws Exception {

    }
}