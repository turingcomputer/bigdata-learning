package com.turing.flink.sink.clickhouse;

//import org.apache.flink.runtime.util.ExecutorThreadFactory;
import com.turing.common.utils.StrUtils;
import org.apache.flink.types.Row;
import org.apache.flink.util.concurrent.ExecutorThreadFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.*;

/**
 * OutputFormat to write Rows into a JDBC database.
 * The OutputFormat has to be configured using the supplied OutputFormatBuilder.
 *
 * @see Row
 * @see DriverManager
 */
public class ClickHouseJDBCOutputFormat extends AbstractClickHouseJDBCOutputFormat<Row> {

    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory.getLogger(ClickHouseJDBCOutputFormat.class);

    // SQL 语句
    private final String query;

    // PreparedStatement
    private final List<PreparedStatement> preparedStatementList;

    // ClickHouse 的集群 IP 和 数据进行绑定存储，记录数据写出的 ClickHouse IP
    private final Map<Integer, List<Row>> ipWithDataList;

    // 满足此时间条件写出数据
    private final long insertCkTimeInterval; // 4000L
    // 插入的批次
    private final int insertCkBatchSize;  // 开发测试用10条

    // 数据库名
    private final String dataBaseName;
    // 数据表名
    private final String tableName;
    // 表列名
    private final String[] tableColumns;

    private transient volatile boolean closed = false;
    private transient ScheduledExecutorService scheduler;
    private transient ScheduledFuture<?> scheduledFuture;
    private transient volatile Exception flushException;

    public ClickHouseJDBCOutputFormat(String username,
                                      String password,
                                      String[] ips,
                                      long insertCkTimeInterval,
                                      int insertCkBatchSize,
                                      String dataBaseName,
                                      String tablename,
                                      String[] tableColums) {
        super(username, password, ips);
        this.insertCkTimeInterval = insertCkTimeInterval;
        this.insertCkBatchSize = insertCkBatchSize;
        this.ipWithDataList = new ConcurrentHashMap<>();
        this.dataBaseName = dataBaseName;
        this.tableName = tablename;
        this.tableColumns = tableColums;
        this.preparedStatementList = Collections.synchronizedList(new ArrayList<>());
        this.query = StrUtils.clickhouseInsertValue(this.tableColumns, this.tableName, this.dataBaseName);
    }

    /**
     * Connects to the target database and initializes the prepared statement.
     *
     * @param taskNumber The number of the parallel instance.
     * @throws IOException Thrown, if the output could not be opened due to an
     *                     I/O problem.
     */
    @Override
    public void open(int taskNumber, int numTasks) throws IOException {
        try {
            establishConnection();

            for (Connection connection : connectionList) {
                PreparedStatement preparedStatement = connection.prepareStatement(query);
                this.preparedStatementList.add(preparedStatement);
            }

            if (insertCkTimeInterval != 0 && insertCkBatchSize != 1) {
                this.scheduler =
                        Executors.newScheduledThreadPool(
                                1, new ExecutorThreadFactory("jdbc-upsert-output-format"));
                this.scheduledFuture =
                        this.scheduler.scheduleWithFixedDelay(
                                () -> {
                                    synchronized (ClickHouseJDBCOutputFormat.this) {
                                        if (!closed) {
                                            try {
                                                LOG.info("ck_flush.............");
                                                flush();
                                            } catch (Exception e) {
                                                flushException = e;
                                            }
                                        }
                                    }
                                },
                                insertCkTimeInterval,
                                insertCkTimeInterval,
                                TimeUnit.MILLISECONDS);
            }

        } catch (SQLException sqe) {
            throw new IllegalArgumentException("open() failed.", sqe);
        } catch (ClassNotFoundException cnfe) {
            throw new IllegalArgumentException("JDBC driver class not found.", cnfe);
        }
    }

    @Override
    public final synchronized void writeRecord(Row row) throws IOException {
        checkFlushException();

        /**
         * 1. 将数据写入CK
         */
        final int[] size = {0};
        ipWithDataList.values().forEach(rows -> {
            size[0] += rows.size();
        });

        if (size[0] >= this.insertCkBatchSize) {
            ipWithDataList.forEach((index, rows) -> {
                try {
                    flush(rows, preparedStatementList.get(index), connectionList.get(index));
                    LOG.info("insertCkBatchSize");
                } catch (SQLException e) {
                    throw new RuntimeException("Preparation of JDBC statement failed.", e);
                }
            });
        }

        /**
         * 将当前行数据添加到List中
         */
        // 轮询写入各个local表，避免单节点数据过多
        if (null != row) {
            Random random = new Random();
            int index = random.nextInt(super.ips.length);

            List<Row> rows = ipWithDataList.get(index);
            if (rows == null) {
                rows = new ArrayList<>();
            }

            rows.add(row);
            ipWithDataList.put(index, rows);
        }
    }

    private void checkFlushException() {
        if (flushException != null) {
            throw new RuntimeException("Writing records to JDBC failed.", flushException);
        }
    }

    // 插入数据
    private synchronized void flush(List<Row> rows,
                                    PreparedStatement preparedStatement,
                                    Connection connection) throws SQLException {
        checkFlushException();

        for (int i = 0; i < rows.size(); ++i) {
            Row row = rows.get(i);
            for (int j = 0; j < this.tableColumns.length; ++j) {

                if (null != row.getField(j)) {
                    preparedStatement.setObject(j + 1, row.getField(j));
                } else {
                    preparedStatement.setObject(j + 1, "null");
                }
            }
            preparedStatement.addBatch();
        }

        preparedStatement.executeBatch();
        connection.commit();
        preparedStatement.clearBatch();

        rows.clear();
    }

    private synchronized void flush() {
        checkFlushException();
        ipWithDataList.forEach((index, rows) -> {
            try {
                flush(rows, preparedStatementList.get(index), connectionList.get(index));
            } catch (SQLException e) {
                // throw new RuntimeException("Preparation of JDBC statement failed.", e);
                flushException = e;
            }
        });
    }

    /**
     * Executes prepared statement and closes all resources of this instance.
     *
     * @throws IOException Thrown, if the input could not be closed properly.
     */
    @Override
    public synchronized void close() throws IOException {
        if (!closed) {
            closed = true;

            if (this.scheduledFuture != null) {
                scheduledFuture.cancel(false);
                this.scheduler.shutdown();
            }

            for (PreparedStatement preparedStatement : this.preparedStatementList) {
                if (preparedStatement != null) {
                    flush();
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        // LOG.info("JDBC statement could not be closed: " + e.getMessage());
                        flushException = e;
                    } finally {
                        preparedStatement = null;
                    }
                }
            }

            closeDbConnection();
        }

        checkFlushException();
    }

}
