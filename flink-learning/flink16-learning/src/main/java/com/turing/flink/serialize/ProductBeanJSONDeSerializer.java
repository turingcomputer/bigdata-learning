package com.turing.flink.serialize;

import com.alibaba.fastjson2.JSON;
import com.turing.bean.ProductBean;
import com.turing.flink.pipeline.examples.FlinkToSingleClickhouseExample001;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.streaming.connectors.kafka.KafkaDeserializationSchema;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @descri {"productId":"a1001","status":"101"}
 * @author lj.michale
 * @date 2023-02-22
 */
public class ProductBeanJSONDeSerializer implements KafkaDeserializationSchema<ProductBean> {

    private static final Logger logger = LoggerFactory.getLogger(FlinkToSingleClickhouseExample001.class);

    private final String encoding = "UTF8";
    private boolean includeTopic;
    private boolean includeTimestamp;

    public ProductBeanJSONDeSerializer(boolean includeTopic, boolean includeTimestamp) {
        this.includeTopic = includeTopic;
        this.includeTimestamp = includeTimestamp;
    }

    @Override
    public TypeInformation<ProductBean> getProducedType() {
        return TypeInformation.of(ProductBean.class);
    }

    @Override
    public boolean isEndOfStream(ProductBean nextElement) {
        return false;
    }

    @Override
    public ProductBean deserialize(ConsumerRecord<byte[], byte[]> consumerRecord) throws Exception {
        if (consumerRecord != null) {
            try {
                String value = new String(consumerRecord.value(), encoding);
                ProductBean product = JSON.parseObject(value, ProductBean.class);
                return product;
            } catch (Exception e) {
                logger.error(">>>>>>deserialize failed : " + e.getMessage(), e);
            }
        }

        return null;
    }
}
