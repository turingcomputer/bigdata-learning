package com.turing.flink.pipeline.sqlgateway;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
/**
 * @descri
 *
 * @author lj.michale
 * @date 2022-12-13
 */
public class FlinkSqlGatewaySubmitJob {

    private static final Logger LOG = LoggerFactory.getLogger(FlinkSqlGatewaySubmitJob.class);

    // 创建 session 的 URL
    private static final String CREATE_SESSION = "http://localhost:8083/v1/sessions";
    // 提交 SQL 的 URL
    private static String SUBMIT_SQL =
            "http://localhost:8083/v1/sessions/${sessionHandle}/statements/";
    // 查询结果的 URL
    private static String QUERY_RESULT =
            "http://localhost:8083/v1/sessions/${sessionHandle}/operations/${operationHandle}/result/0";

    public static void main(String[] args) throws IOException {
        // 创建一个 session
        String create_session = httpPost(CREATE_SESSION, "");
        JSONObject jsonObject = JSON.parseObject(create_session);
        // 获取 sessionHandle
        String sessionHandle = jsonObject.getString("sessionHandle");
        System.out.println(create_session);
        SUBMIT_SQL = SUBMIT_SQL.replace("${sessionHandle}", sessionHandle);
        String OrdersDDL =
                "CREATE TABLE Orders (order_number BIGINT,price DECIMAL(32,2),buyer ROW<first_name STRING, last_name STRING>,order_time TIMESTAMP(3)) WITH('connector'='datagen','rows-per-second'='1')";
        String ResultDDL =
                "CREATE TABLE Result_table (order_number BIGINT,price DECIMAL(32,2),buyer ROW<first_name STRING, last_name STRING>,order_time TIMESTAMP(3)) WITH('connector'='print')";
        String DML = "insert into Result_table select * from Orders";
        // 创建 Source Table
        String submitSQL1 = httpPost(SUBMIT_SQL, MapToString(OrdersDDL));
        System.out.println(submitSQL1);
        // 创建 Sink Table
        String submitSQL2 = httpPost(SUBMIT_SQL, MapToString(ResultDDL));
        System.out.println(submitSQL2);
        // 执行 INSERT INTO 语句
        String submitSQL3 = httpPost(SUBMIT_SQL, MapToString(DML));
        System.out.println(submitSQL3);

        JSONObject jsonObject1 = JSON.parseObject(submitSQL3);
        String operationHandle = jsonObject1.getString("operationHandle");
        QUERY_RESULT =
                QUERY_RESULT
                        .replace("${sessionHandle}", sessionHandle)
                        .replace("${operationHandle}", operationHandle);
        String result = httpGet(QUERY_RESULT);
        System.out.println(result);
    }

    public static String httpPost(String url, String json) throws IOException {
        OkHttpClient httpClient = new OkHttpClient().newBuilder().build();
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, json);
        Request request =
                new Request.Builder()
                        .url(url)
                        // .addHeader("Authorization", "Basic " + authorization)
                        .post(body)
                        .build();
        Response response = httpClient.newCall(request).execute();
        return response.body().string();
    }

    public static String httpGet(String url) throws IOException {
        OkHttpClient httpClient = new OkHttpClient().newBuilder().build();
        Request request = new Request.Builder().url(url).get().build();
        Response response = httpClient.newCall(request).execute();
        return response.body().string();
    }

    public static String MapToString(String ddl) {
        Map<String, String> paramsMap = new HashMap<>();
        paramsMap.put("statement", ddl);
        return JSON.toJSONString(paramsMap);
    }
}
