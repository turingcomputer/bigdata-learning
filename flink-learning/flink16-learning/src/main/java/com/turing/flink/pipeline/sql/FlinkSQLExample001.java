package com.turing.flink.pipeline.sql;

import org.apache.flink.api.common.RuntimeExecutionMode;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.TableResult;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

/**
 * @descri
 *
 * @author lj.michale
 * @date 2022-12-13
 */
public class FlinkSQLExample001 {

    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.enableCheckpointing(3000);
        env.setRuntimeMode(RuntimeExecutionMode.STREAMING);
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        // 通过FlinkSQL获取kafka数据
        // 定义 DDL
        String mysqlSourceDDL = "CREATE TABLE kafka_source (\n" +
                "    funcName STRING,\n" +
                "    data ROW<snapshots ARRAY<ROW<content_type STRING,url STRING>>,audio ARRAY<ROW<content_type STRING,url STRING>>>,\n" +
                "    resultMap ROW<`result` MAP<STRING,STRING>,isSuccess BOOLEAN>,\n" +
                "    meta  MAP<STRING,STRING>,\n" +
                "    `type` INT,\n" +
                "    `timestamp` BIGINT,\n" +
                "    arr ARRAY<ROW<address STRING,city STRING>>,\n" +
                "    map MAP<STRING,INT>,\n" +
                "    doublemap MAP<STRING,MAP<STRING,INT>>,\n" +
                "    proctime as PROCTIME()\n" +
                ") WITH (\n" +
                "    'connector' = 'kafka', -- 使用 kafka connector\n" +
                "    'topic' = 'turing-log',  -- kafka topic\n" +
                "    'properties.bootstrap.servers' = 'localhost:9092',  -- broker连接信息\n" +
                "    'properties.group.id' = 'jason_flink_test', -- 消费kafka的group_id\n" +
                "    'scan.startup.mode' = 'latest-offset',  -- 读取数据的位置\n" +
                "    'format' = 'json',  -- 数据源格式为 json\n" +
                "    'json.fail-on-missing-field' = 'true', -- 字段丢失任务不失败\n" +
                "    'json.ignore-parse-errors' = 'false'  -- 解析失败跳过\n" +
                ");";

        tableEnv.executeSql(mysqlSourceDDL);
        TableResult result = tableEnv.executeSql("select\n" +
                "funcName,\n" +
                "doublemap['inner_map']['key'],\n" +
                "count(data.snapshots[1].url),\n" +
                "`type`,\n" +
                "TUMBLE_START(proctime, INTERVAL '30' second) as t_start\n" +
                "from kafka_source\n" +
                "group by TUMBLE(proctime, INTERVAL '30' second),funcName,`type`,doublemap['inner_map']['key']");
        result.print();

        env.execute("FlinkSQLExample001");

    }

}
