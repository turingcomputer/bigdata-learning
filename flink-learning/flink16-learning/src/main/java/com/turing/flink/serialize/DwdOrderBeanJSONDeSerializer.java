package com.turing.flink.serialize;

import com.alibaba.fastjson2.JSON;
import com.turing.bean.DwdOrderBean;
import org.apache.flink.api.common.serialization.DeserializationSchema;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.streaming.connectors.kafka.KafkaDeserializationSchema;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @descri {"productId":"a1001","status":"101"}
 * @author lj.michale
 * @date 2023-02-22
 */
public class DwdOrderBeanJSONDeSerializer implements KafkaDeserializationSchema<DwdOrderBean> {

    private static final Logger logger = LoggerFactory.getLogger(MyKafkaDeserialization.class);
    private final String encoding = "UTF8";
    private boolean includeTopic;
    private boolean includeTimestamp;

    public DwdOrderBeanJSONDeSerializer(boolean includeTopic, boolean includeTimestamp) {
        this.includeTopic = includeTopic;
        this.includeTimestamp = includeTimestamp;
    }

    @Override
    public TypeInformation<DwdOrderBean> getProducedType() {
        return TypeInformation.of(DwdOrderBean.class);
    }

    @Override
    public void open(DeserializationSchema.InitializationContext context) throws Exception {

    }

    @Override
    public boolean isEndOfStream(DwdOrderBean nextElement) {
        return false;
    }

    @Override
    public DwdOrderBean deserialize(ConsumerRecord<byte[], byte[]> consumerRecord) throws Exception {
        if (consumerRecord != null) {
            try {
                String value = new String(consumerRecord.value(), encoding);
                DwdOrderBean dwdOrderBean = JSON.parseObject(value, DwdOrderBean.class);
                return dwdOrderBean;
            } catch (Exception e) {
                logger.error("deserialize failed : " + e.getMessage());
            }
        }

        return null;
    }


}
