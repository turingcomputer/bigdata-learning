package com.turing.flink.pipeline.sql;

import org.apache.flink.api.common.RuntimeExecutionMode;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.TableResult;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

/**
 * @descri
 *
 * @author lj.michale
 * @date 2022-12-13
 */
public class FlinkSQLExample002 {

    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.enableCheckpointing(3000);
        env.setRuntimeMode(RuntimeExecutionMode.STREAMING);
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        // 通过FlinkSQL获取kafka数据
        // 定义 DDL
        String mysqlSourceDDL = "create table dim_mysql_games (\n" +
                "  ts_code STRING,\n" +
                "  trade_date STRING,\n" +
                "  turnover_rate BIGINT,\n" +
                "  volume_ratio BIGINT,\n" +
                "  pe BIGINT,\n" +
                "  pb BIGINT\n" +
                ") with (\n" +
                "  'connector' = 'jdbc',\n" +
                "  'url' = 'jdbc:mysql://localhost:3306/bigdata?useSSL=false&useUnicode=true&characterEncoding=UTF-8&characterSetResults=UTF-8&zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC',\n" +
                "  'username' = 'root',\n" +
                "  'password' = 'Turing@2022',\n" +
                "  'table-name' = 'history',\n" +
                "  'driver' = 'com.mysql.cj.jdbc.Driver',\n" +
                "  'scan.fetch-size' = '100',\n" +
                "  'lookup.cache.max-rows' = '100',\n" +
                "  'lookup.cache.ttl' = '300s'\n" +
                ")\n";
        tableEnv.executeSql(mysqlSourceDDL);
        TableResult result = tableEnv.executeSql("select * from dim_mysql_games");
        result.print();


        env.execute("FlinkSQLExample002");

    }

}
