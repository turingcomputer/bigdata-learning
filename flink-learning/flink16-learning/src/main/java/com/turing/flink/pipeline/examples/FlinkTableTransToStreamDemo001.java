//package com.turing.flink.pipeline.examples;
//
//import com.alibaba.fastjson2.JSON;
//import com.alibaba.fastjson2.JSONArray;
//import com.alibaba.fastjson2.JSONObject;
//import org.apache.flink.api.common.functions.MapFunction;
//import org.apache.flink.streaming.api.datastream.DataStream;
//import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
//import org.apache.flink.streaming.api.functions.source.datagen.DataGeneratorSource;
//import org.apache.flink.table.api.Table;
//import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
//
///**
// * @descri
// *
// * @author lj.michale
// * @date 2023-03-03
// */
//public class FlinkTableTransToStreamDemo001 {
//
//    public static void main(String[] args) throws Exception {
//        StreamExecutionEnvironment env = FlinkEnv.FlinkDataStreamRunEnv();
//        env.setParallelism(1);
//        StreamTableEnvironment tableEnv = FlinkEnv.getStreamTableEnv(env);
//
//        DataGeneratorSource<String> dataGeneratorSource = new DataGeneratorSource<>(new DataGeneratorImpl003());
//
//        DataStream<String> sourceStream = env.addSource(dataGeneratorSource).returns(String.class);
//
////        sourceStream.print("source");
//        // map函数将数据转换为POJO类
//        DataStream<Event> mapStream = sourceStream.map(new MapFunction<String, Event>() {
//            @Override
//            public Event map(String s) throws Exception {
//                JSONObject jsonObject = JSON.parseObject(s);
//                String name = jsonObject.getString("name");
//                JSONObject title = jsonObject.getJSONObject("title");
//                String title_name = title.getString("title_name");
//                int title_number = title.getIntValue("title_number");
//                JSONArray user_info = jsonObject.getJSONArray("user_info");
//                String address = user_info.getJSONObject(0).getString("address");
//                JSONObject time_info = jsonObject.getJSONObject("time_info");
//                long timestamp = time_info.getLongValue("timestamp");
//                return new Event(name, title.toJSONString(), title_name, title_number, user_info.toJSONString(), address, time_info.toJSONString(), timestamp);
//            }
//        }).returns(Event.class);
//
//        mapStream.print("map source");
//        // 将POJO类的数据流转换为Table，由于是POJO类的数据流，所以转换后的Table的column信息跟POJO类属性信息一致
//        Table table = tableEnv.fromDataStream(mapStream);
//        // 创建临时表，表名为source
//        tableEnv.createTemporaryView("source", table);
//        // 执行sql，这里可以直接使用POJO类属性名来查询
//        Table result = tableEnv.sqlQuery("select name, title_name, address from source");
//        // 将结果表转换为DataStream来输出
//        tableEnv.toDataStream(result).print("result");
//        env.execute("job start");
//    }
//}
