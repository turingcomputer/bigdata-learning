package com.turing.flink.window;
/**
 * @descri Data for SessionWindowingITCase
 *
 * @author lj.michale
 * @date 2023-02-27
 */
public class SessionWindowingData {

    public static final String EXPECTED =
            "(a,1,1)\n" + "(c,6,1)\n" + "(c,11,1)\n" + "(b,1,3)\n" + "(a,10,1)";

    private SessionWindowingData() {}
}
