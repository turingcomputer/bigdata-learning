package com.turing.flink.pipeline.sql;

import org.apache.flink.api.common.RuntimeExecutionMode;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.TableConfig;
import org.apache.flink.table.api.TableEnvironment;
import org.apache.flink.table.api.TableResult;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

/**
 * @descri
 *
 * @author lj.michale
 * @date 2022-12-13
 */
public class FlinkSQLExample003 {

    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.enableCheckpointing(3000);
//        env.setRuntimeMode(RuntimeExecutionMode.STREAMING);
//        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        // 实例化 table environment
        Configuration configuration = new Configuration();
        // 设置底层 key-value 选项
        configuration.setString("table.exec.mini-batch.enabled", "true");
        configuration.setString("table.exec.mini-batch.allow-latency", "5 s");
        configuration.setString("table.exec.mini-batch.size", "5000");
        EnvironmentSettings settings = EnvironmentSettings.newInstance()
                .inStreamingMode().withConfiguration(configuration).build();
        TableEnvironment tEnv = TableEnvironment.create(settings);

        // 实例化表环境之后访问 flink configuraion
        TableConfig tableConfig = tEnv.getConfig();
        // 设置底层 key-value 选项
        configuration.setString("table.exec.mini-batch.enabled", "true");
        configuration.setString("table.exec.mini-batch.allow-latency", "5 s");
        configuration.setString("table.exec.mini-batch.size", "5000");



        env.execute("FlinkSQLExample003");

    }

}
