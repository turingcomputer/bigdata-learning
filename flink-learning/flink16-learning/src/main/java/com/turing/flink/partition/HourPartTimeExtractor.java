//package com.turing.flink.partition;
//
//import java.sql.Timestamp;
//import java.time.LocalDateTime;
//import java.util.List;
//
///**
// * @descri
// *
// * @author lj.michale
// * @date 2022-12-22
// */
//public class HourPartTimeExtractor implements PartitionTimeExtractor {
//    @Override
//    public LocalDateTime extract(List<String> keys, List<String> values) {
//        String dt = values.get(0);
//        String hour = values.get(1);
//        return Timestamp.valueOf(dt + " " + hour + ":00:00").toLocalDateTime();
//    }
//}
