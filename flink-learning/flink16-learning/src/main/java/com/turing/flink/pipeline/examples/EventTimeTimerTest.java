package com.turing.flink.pipeline.examples;

import com.turing.bean.ClickEvent;
import com.turing.flink.source.ClickSource;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.util.Collector;

import java.sql.Timestamp;
import java.time.Duration;

/**
 * @descri
 *
 * @author lj.michale
 * @date 2023-02-28
 */
public class EventTimeTimerTest {

    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        // 读取数据，并提取时间戳、生成水位线
        SingleOutputStreamOperator<ClickEvent> stream = env.addSource(new ClickSource())
                .assignTimestampsAndWatermarks(WatermarkStrategy.<ClickEvent>forBoundedOutOfOrderness(Duration.ZERO)
                        .withTimestampAssigner(new SerializableTimestampAssigner<ClickEvent>() {
                            @Override
                            public long extractTimestamp(ClickEvent element, long recordTimestamp) {
                                return element.timestamp;
                            }
                        }));

        //事件时间定时器
        stream.keyBy(data -> data.user)
                .process(new KeyedProcessFunction<String, ClickEvent, String>() {
                    @Override
                    public void processElement(ClickEvent value, KeyedProcessFunction<String, ClickEvent, String>.Context ctx, Collector<String> out) throws Exception {
                        // 获取数据当前的时间
                        long currTs = ctx.timestamp();
                        out.collect(ctx.getCurrentKey() + "数据到达,时间戳：" + new Timestamp(currTs) + " watermark：" + ctx.timerService().currentWatermark());
                        //注册一个10s之后的定时器
                        ctx.timerService().registerEventTimeTimer(currTs + 10 * 1000L);
                    }

                    @Override
                    public void onTimer(long timestamp, OnTimerContext ctx, Collector<String> out) throws Exception {
                        System.err.println(ctx.getCurrentKey() + "定时器触发,触发时间：" + new Timestamp(timestamp) + " watermark：" + ctx.timerService().currentWatermark());
                        // out.collect();
                    }

                }).print();

        env.execute();
    }
}