package com.turing.flink.pipeline.watermark;

import com.turing.bean.EventBean2;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.time.Duration;

/**
 * @descri
 *
 * @author lj.michale
 * @date 2023-01-13
 */
public class FlinkWatermarkExample001 {

    public static void main(String[] args) {

        Configuration config = new Configuration();
        config.setInteger("rest.port", 8081);

        StreamExecutionEnvironment env = StreamExecutionEnvironment.createLocalEnvironmentWithWebUI(config);
        DataStreamSource<String> source = env.socketTextStream("192.168.10.100", 9999);

        SingleOutputStreamOperator<EventBean2> beanStream = source.map(s -> {
            String[] split = s.split(",");
            return new EventBean2(Long.parseLong(split[0]),Long.parseLong(split[1]) ,Integer.parseInt(split[2]));
        }).returns(EventBean2.class);

        SingleOutputStreamOperator<EventBean2> watermarkedStream = beanStream
                .assignTimestampsAndWatermarks(
                        WatermarkStrategy.<EventBean2>forBoundedOutOfOrderness(Duration.ofMillis(0))
                        .withTimestampAssigner(new SerializableTimestampAssigner<EventBean2>() {
                            @Override
                            public long extractTimestamp(EventBean2 eventBean, long l) {
                                return eventBean.getTimestamp();
                            }
                        })
        );

    }

}
