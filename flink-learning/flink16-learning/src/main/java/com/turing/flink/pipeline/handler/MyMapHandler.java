package com.turing.flink.pipeline.handler;

import com.alibaba.fastjson2.JSON;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.kafka.connect.source.SourceRecord;
import org.apache.kafka.connect.data.Struct;

/**
 * @descri
 *
 * @author lj.michale
 * @date 2022-12-08
 */
class MyMapHandler implements MapFunction<String, Object> {

    @Override
    public Object map(String s) throws Exception {
        SourceRecord sourceRecord = JSON.toJavaObject(JSON.parseObject(s), SourceRecord.class);
        Struct struct = (Struct)sourceRecord.value();
        System.out.println("struct:" + struct);

        return s;
    }

}
