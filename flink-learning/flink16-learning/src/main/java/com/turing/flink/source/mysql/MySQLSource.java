package com.turing.flink.source.mysql;

import com.turing.bean.Student;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.source.RichParallelSourceFunction;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.concurrent.TimeUnit;

/**
 * @descri
 *
 * @author lj.michale
 * @date 2023-02-27
 */
public class MySQLSource extends RichParallelSourceFunction<Student> {

    private Connection conn = null;

    private PreparedStatement ps = null;

    @Override
    public void open(Configuration parameters) throws Exception {
        //加载驱动,开启连接
        Class.forName("com.mysql.jdbc.Driver");
        conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/bigdata", "root", "Turing@2022");
        String sql = "select id,name,age from t_student";
        ps = conn.prepareStatement(sql);
    }

    private boolean flag = true;

    @Override
    public void run(SourceContext<Student> ctx) throws Exception {
        while (flag) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                int age = rs.getInt("age");
                ctx.collect(new Student(id, name, age));
            }

            TimeUnit.SECONDS.sleep(5);
        }
    }

    @Override
    public void cancel() {
        flag = false;
    }

    @Override
    public void close() throws Exception {
        if (conn != null) {
            conn.close();
        }
        if (ps != null) {
            ps.close();
        }
    }
}
