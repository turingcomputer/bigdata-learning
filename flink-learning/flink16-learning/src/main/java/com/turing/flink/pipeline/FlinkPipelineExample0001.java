package com.turing.flink.pipeline;

import com.turing.common.constant.MysqlPropConstant;
import org.apache.flink.api.common.RuntimeExecutionMode;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.TableResult;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

/**
 * @descri
 *
 * @author lj.michale
 * @date 2022-12-08
 */
public class FlinkPipelineExample0001 {

    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.enableCheckpointing(3000);
        env.setRuntimeMode(RuntimeExecutionMode.STREAMING);
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        String tableName = "push_sendmsg_sink";

        String mysqlSourceDDL = String.format("CREATE TABLE `push_sendmsg_sink` (\n" +
                " uid STRING ,\n" +
                " password  STRING,\n" +
                " imei  STRING,\n" +
                " pkgname  STRING,\n" +
                " appkey STRING, \n" +
                " primary key (uid)  NOT  ENFORCED \n" +
                ") WITH (\n" +
                "'connector.type' = 'jdbc', -- 连接方式\n" +
                "'connector.url' = '%s', -- jdbc的url\n" +
                "'connector.table' = '%s',  -- 表名\n" +
                "'connector.driver' = 'com.mysql.jdbc.Driver', -- 驱动名字，可以不填，会自动从上面的jdbc url解析 \n" +
                "'connector.username' = '%s', --  用户名\n" +
                "'connector.password' = '%s',  -- 密码\n" +
                "//'connector.write.unique-key' = 'uid',  -- 要更新的主键\n" +
                "'connector.write.max-retries' = '3',-- 写入失败时，重试3次\n" +
                "'connector.write.flush.max-rows' = '100', -- 意思是攒满多少条才触发写入 \n" +
                "'connector.write.flush.interval' = '1s' -- 意思是攒满多少秒才触发写入；这2个参数，无论数据满足哪个条件，就会触发写入\n" +
                ");", MysqlPropConstant.MYSQL_HOSTS, tableName, MysqlPropConstant.MYSQL_USER, MysqlPropConstant.MYSQL_PASSWORD);

        tableEnv.executeSql(mysqlSourceDDL);
        TableResult result = tableEnv.executeSql("SELECT * FROM push_sendmsg_sink");
        result.print();

        env.execute("FlinkPipelineExample001");

    }


}
