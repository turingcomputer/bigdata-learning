package com.turing.flink.function.process;

import org.apache.flink.api.common.state.MapState;
import org.apache.flink.api.common.state.MapStateDescriptor;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.util.Collector;

/**
 * @descri
 *
 * @author lj.michale
 * @date 2023-02-28
 */
public class WordCountProcessFunction extends ProcessFunction<String, Tuple2<String, Long>> {
    private MapState<String, Long> countState;

    @Override
    public void open(Configuration parameters) throws Exception {
        countState = getRuntimeContext().getMapState(new MapStateDescriptor<>("counts", String.class, Long.class));
    }

    @Override
    public void processElement(String word,
                               Context context,
                               Collector<Tuple2<String, Long>> collector) throws Exception {
        Long count = countState.get(word);
        if (count == null) {
            count = 0L;
        }

        countState.put(word, count + 1);
        collector.collect(new Tuple2<>(word, count + 1));
    }
}
