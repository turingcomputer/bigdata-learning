package com.turing.flink.pipeline.examples;

import com.turing.common.utils.FlinkEnvUtils;
import com.turing.flink.function.Splitter;
import com.turing.flink.function.process.CountWithTimeoutFunction;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.api.java.tuple.Tuple1;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.streaming.api.datastream.DataStream;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.util.Collector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;

/**
 * @descri
 *
 * @author lj.michale
 * @date 2023-02-28
 */
public class KeyedProcessFunctionExample1 {

    private static final Logger logger = LoggerFactory.getLogger(ProcessFunctionExample001.class);
    // pipeline全局配置文件
    private static final String propertiesFilePath = "E:\\company\\Turing\\bigdata\\bigdata-learning\\flink-learning\\flink16-learning\\src\\main\\resources\\application-dev.properties";

    public static void main(String[] args) throws Exception {

        ParameterTool parameter = ParameterTool.fromPropertiesFile(propertiesFilePath);
        final StreamExecutionEnvironment env = FlinkEnvUtils.getStreamExecutionEnv(parameter);
        env.setParallelism(1);
        // 处理时间,也被废弃
//        env.setStreamTimeCharacteristic(TimeCharacteristic.ProcessingTime);
        // 监听本地9999端口，读取字符串
        DataStream<String> socketDataStream = env.socketTextStream("192.168.10.102", 9999);

        // 所有输入的单词，如果超过10秒没有再次出现，都可以通过CountWithTimeoutFunction得到
//        DataStream<Tuple2<String, Long>> timeOutWord = socketDataStream
//                // 对收到的字符串用空格做分割，得到多个单词
//                .flatMap(new Splitter())
//                // 设置时间戳分配器，用当前时间作为时间戳
//                .assignTimestampsAndWatermarks(new AssignerWithPeriodicWatermarks<Tuple2<String, Integer>>() {
//
//                    @Override
//                    public long extractTimestamp(Tuple2<String, Integer> element, long previousElementTimestamp) {
//                        // 使用当前系统时间作为时间戳
//                        return System.currentTimeMillis();
//                    }
//
//                    @Override
//                    public Watermark getCurrentWatermark() {
//                        // 本例不需要watermark，返回null
//                        return null;
//                    }
//                })
//                // 将单词作为key分区
//                .keyBy(0)
//                // 按单词分区后的数据，交给自定义KeyedProcessFunction处理
//                .process(new CountWithTimeoutFunction());
        DataStream<Tuple2<String, Long>> timeOutWord = socketDataStream
                // 对收到的字符串用空格做分割，得到多个单词
                .flatMap(new Splitter())
                .assignTimestampsAndWatermarks(WatermarkStrategy.<Tuple2<String, Integer>>forBoundedOutOfOrderness(Duration.ZERO)
                        .withTimestampAssigner(new SerializableTimestampAssigner<Tuple2<String, Integer>>() {
                            @Override
                            public long extractTimestamp(Tuple2<String, Integer> stringIntegerTuple2, long previousElementTimestamp) {
                                // 使用当前系统时间作为时间戳
                                return System.currentTimeMillis();
                            }
                        }))
                .keyBy(0).process(new CountWithTimeoutFunction());
                // 或者.keyBy(tuple -> tuple.f0) // 修改后，使用tuple.f0表示第0个位置的信息
        //.process(new CountWithTimeoutFunction());

        // 所有输入的单词，如果超过10秒没有再次出现，就在此打印出来
        timeOutWord.print();


        env.execute("KeyedProcessFunctionExample1");

    }
}
