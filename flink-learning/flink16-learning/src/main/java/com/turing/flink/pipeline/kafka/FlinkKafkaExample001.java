package com.turing.flink.pipeline.kafka;

import com.turing.bean.Jason;
import com.turing.flink.serialize.MyKafkaDeserialization;
import org.apache.flink.api.common.restartstrategy.RestartStrategies;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.connector.kafka.source.reader.deserializer.KafkaRecordDeserializationSchema;
import org.apache.flink.runtime.state.hashmap.HashMapStateBackend;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @descri
 *
 * @author lj.michale
 * @date 2022-12-13
 */
public class FlinkKafkaExample001 {

    private static final Logger logger = LoggerFactory.getLogger(FlinkKafkaExample001.class);

    public static void main(String[] args) throws Exception {

        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        // 开启事件时间语义
        env.enableCheckpointing(200, CheckpointingMode.EXACTLY_ONCE);
        env.getCheckpointConfig().setMinPauseBetweenCheckpoints(30000);
        env.getCheckpointConfig().setCheckpointTimeout(10000);
        env.getCheckpointConfig().setMaxConcurrentCheckpoints(1);
        // 使用 externalized checkpoints，这样 checkpoint 在作业取消后仍就会被保留(如果使用savepoint取消任务的化就不用设置此参数，开启可以防止一些失误操作，不过也要注意自己手动删除记录)
        //ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION：当作业取消时，保留作业的 checkpoint。注意，这种情况下，需要手动清除该作业保留的 checkpoint。
        //ExternalizedCheckpointCleanup.DELETE_ON_CANCELLATION：当作业取消时，删除作业的 checkpoint。仅当作业失败时，作业的 checkpoint 才会被保留。
        env.getCheckpointConfig().setExternalizedCheckpointCleanup(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
        env.setRestartStrategy(RestartStrategies.fixedDelayRestart(3, 1000));
        // 状态后端-HashMapStateBackend
        env.setStateBackend(new HashMapStateBackend());
        // 等价于MemoryStateBackend
        env.getCheckpointConfig().setCheckpointStorage("file:///E:\\company\\Turing\\bigdata\\bigdata-learning\\checkpoint");

        String username = "root";
        String password = "Turiing@2022";

        KafkaSource<Jason> source = KafkaSource.<Jason>builder()
                .setProperty("security.protocol", "SASL_PLAINTEXT")
                .setProperty("sasl.mechanism", "PLAIN")
                .setProperty("sasl.jaas.config", "org.apache.kafka.common.security.plain.PlainLoginModule required username=\"" + username + "\" password=\"" + password + "\";")
                // discover new partitions per 10 seconds
                .setProperty("partition.discovery.interval.ms", "10000")
                .setBootstrapServers("broker")
                .setTopics("topic")
                .setGroupId("group_id")
                .setStartingOffsets(OffsetsInitializer.earliest())
                .setDeserializer(KafkaRecordDeserializationSchema.of(new MyKafkaDeserialization(true, true)))
                // 只反序列化 value
//                .setValueOnlyDeserializer(new MyDeSerializer())
                .build();

        env.execute("FlinkKafkaExample001");
    }
}
