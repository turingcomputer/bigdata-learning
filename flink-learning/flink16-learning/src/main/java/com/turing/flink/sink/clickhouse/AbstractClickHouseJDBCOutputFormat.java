package com.turing.flink.sink.clickhouse;
/**
 * @descri
 *
 * @author lj.michale
 * @date 2023-02-22
 */
import org.apache.flink.api.common.io.RichOutputFormat;
import org.apache.flink.configuration.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * 写出数据到 ClickHouse
 *
 * @param <T>
 */
public abstract class AbstractClickHouseJDBCOutputFormat<T> extends RichOutputFormat<T> {

    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory.getLogger(AbstractClickHouseJDBCOutputFormat.class);

    // ClickHouse 用户名
    private final String username;

    // ClickHouse 密码
    private final String password;

    // ClickHouse JDBC 驱动类
    private final String drivername = "ru.yandex.clickhouse.ClickHouseDriver";

    // ClickHouse 集群 所有IP
    public final String[] ips;

    // ClickHouse JDBC 连接集合
    protected final List<Connection> connectionList;

    /**
     * 构造方法
     *
     * @param username ClickHouse 用户名
     * @param password ClickHouse 密码
     * @param ips      ClickHouse 集群 所有IP
     */
    public AbstractClickHouseJDBCOutputFormat(String username,
                                              String password, String[] ips) {
        this.username = username;
        this.password = password;
        this.ips = ips;
        this.connectionList = new ArrayList<>();
    }

    /**
     * 创建 ClickHouse JDBC 连接
     *
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    protected void establishConnection() throws SQLException, ClassNotFoundException {
        Class.forName(drivername);

        for (String ip : this.ips) {
            String url = "jdbc:clickhouse://" + ip;
            Connection connection = DriverManager.getConnection(url, this.username, this.password);
            this.connectionList.add(connection);
        }
    }

    @Override
    public void configure(Configuration parameters) {

    }

    /**
     * 关闭 ClickHouse JDBC 连接
     *
     * @throws IOException
     */
    protected void closeDbConnection() throws IOException {
        for (Connection connection : this.connectionList) {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException se) {
                    LOG.warn("JDBC connection could not be closed: " + se.getMessage());
                } finally {
                    connection = null;
                }
            }
        }
    }
}
