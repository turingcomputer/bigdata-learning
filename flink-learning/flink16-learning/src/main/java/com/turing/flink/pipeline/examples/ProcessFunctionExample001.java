package com.turing.flink.pipeline.examples;

import com.turing.common.utils.FlinkEnvUtils;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.TimerService;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.util.Collector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @descri
 *
 * @author lj.michale
 * @date 2023-02-27
 */
public class ProcessFunctionExample001 {

    private static final Logger logger = LoggerFactory.getLogger(ProcessFunctionExample001.class);
    // pipeline全局配置文件
    private static final String propertiesFilePath = "E:\\company\\Turing\\bigdata\\bigdata-learning\\flink-learning\\flink16-learning\\src\\main\\resources\\application-dev.properties";

    public static void main(String[] args) throws Exception {

        ParameterTool parameter = ParameterTool.fromPropertiesFile(propertiesFilePath);

        final StreamExecutionEnvironment env = FlinkEnvUtils.getStreamExecutionEnv(parameter);
        DataStream<String> dataStream = env.socketTextStream("192.168.10.102",9999);

        /// 使用 map 函数将获取到的每一行数据以空格分割，每个单词计数
        dataStream.flatMap(new FlatMapFunction<String, Tuple2<String, Integer>>() {
            @Override
            public void flatMap(String s, Collector<Tuple2<String, Integer>> collector) throws Exception {
                String[] arr = s.split(" ");
                for(String word: arr){
                    collector.collect(new Tuple2<>(word, 1));
                }
            }
        }).keyBy(t->t.f0)
          .process(new KeyedProcessFunction<String, Tuple2<String, Integer>, Tuple2<String, Integer> >() {
            // 定义state存储中间结果
            private ValueState<Integer> accState;

            @Override
            public void open(Configuration parameters) throws Exception {
                //定义 state 描述符
                ValueStateDescriptor<Integer> wordDescriptor = new ValueStateDescriptor<>(
                        "sum-state",
                        Integer.class
                );

                accState = getRuntimeContext().getState(wordDescriptor);
                super.open(parameters);
            }

            // timer的定义，这里使用默认实现
            @Override
            public void onTimer(long timestamp, OnTimerContext ctx,
                                Collector<Tuple2<String, Integer>> out) throws Exception {
                super.onTimer(timestamp, ctx, out);
            }

            // 每个元素处理
            @Override
            public void processElement(Tuple2<String, Integer> value,
                                       Context ctx,
                                       Collector<Tuple2<String, Integer>> out) throws Exception {
                // 输出元数据信息
                TimerService timerService = ctx.timerService();
                //System.out.println("currentProcessingTime：" +
                //timerService.currentProcessingTime() +
                // "，currentWatermark：" + timerService.currentWatermark());
                //System.out.println("Key：" + ctx.getCurrentKey() +
                // "，input element：" + value);

                // 输出元数据信息
                Integer accValue = accState.value();
                if(accValue == null) {
                    accState.update(value.f1);
                } else {
                    accState.update(accValue + value.f1);
                }

                // 输出 word 统计结果
                out.collect(new Tuple2<>(value.f0, accState.value()));
            }
        }).print();

        env.execute("ProcessFunctionExample001");

    }

}
