package com.turing.flink.sink.clickhouse;

import org.apache.flink.api.common.functions.RuntimeContext;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
import org.apache.flink.types.Row;

/**
 * @descri 数据写出到 ClickHouse
 *
 * @author lj.michale
 * @date 2023-02-22
 */
public class ClickHouseJDBCSinkFunction extends RichSinkFunction<Row> {

    final ClickHouseJDBCOutputFormat outputFormat;

    public ClickHouseJDBCSinkFunction(ClickHouseJDBCOutputFormat outputFormat) {
        this.outputFormat = outputFormat;
    }

    /**
     * 数据写出
     *
     * @param value   数据
     * @param context Flink Context
     * @throws Exception
     */
    @Override
    public void invoke(Row value, Context context) throws Exception {
        outputFormat.writeRecord(value);
    }

    /**
     * 初始化参数化
     * @param parameters
     * @throws Exception
     */
    @Override
    public void open(Configuration parameters) throws Exception {
        super.open(parameters);
        RuntimeContext ctx = getRuntimeContext();
        outputFormat.setRuntimeContext(ctx);
        outputFormat.open(ctx.getIndexOfThisSubtask(), ctx.getNumberOfParallelSubtasks());
    }

    /**
     * 停止写出数据
     * @throws Exception
     */
    @Override
    public void close() throws Exception {
        outputFormat.close();
        super.close();
    }
}
