package com.turing.flink.pipeline.examples;

import com.turing.common.utils.FlinkEnvUtils;
import com.turing.flink.pipeline.examples.ProcessFunctionExample001;
import org.apache.flink.api.common.RuntimeExecutionMode;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @descri
 *
 * @author lj.michale
 * @date 2023-02-27
 */
public class RichAsyncFunctionExample001 {

    private static final Logger logger = LoggerFactory.getLogger(ProcessFunctionExample001.class);
    private static final String propertiesFilePath = "E:\\company\\Turing\\bigdata\\bigdata-learning\\flink-learning\\flink16-learning\\src\\main\\resources\\application-dev.properties";

    public static void main(String[] args) throws Exception {
        ParameterTool parameter = ParameterTool.fromPropertiesFile(propertiesFilePath);
        final StreamExecutionEnvironment env = FlinkEnvUtils.getStreamExecutionEnv(parameter);
        env.setParallelism(1);
        env.setRuntimeMode(RuntimeExecutionMode.STREAMING);

        env.execute("RichAsyncFunctionExample001");
    }

}
