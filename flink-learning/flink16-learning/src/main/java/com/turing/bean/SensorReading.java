package com.turing.bean;
/**
 * @descri
 *
 * @author lj.michale
 * @date 2023-03-01
 */
public class SensorReading {

    private String id;

    private long timestamp;

    private double temperature;

    public SensorReading(String id, long timestamp, double temperature) {
        this.id = id;
        this.timestamp = timestamp;
        this.temperature = temperature;
    }
}
