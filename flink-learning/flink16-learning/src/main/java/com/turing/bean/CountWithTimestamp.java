package com.turing.bean;
/**
 * @descri 保存在key状态中
 *
 * @author lj.michale
 * @date 2023-02-28
 */
public class CountWithTimestamp {
    public String key;

    public long count;

    public long lastModified;
}
