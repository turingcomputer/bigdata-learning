package com.turing.common.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @descri
 *
 * @author lj.michale
 * @date 2022-12-02
 */
public class PropertiesUtils {

    private static Properties p = new Properties();
    private static ReentrantLock lock = new ReentrantLock();

    public static void load(String path) {
        InputStream in = null;
        lock.lock();

        try {
            path = PropertiesUtils.class.getClassLoader().getResource("") + path;
            path = path.replaceAll("file:/", "");
            path = path.replaceAll("20%", " ");
            in = new FileInputStream(path);
            p.load(in);
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(in!=null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            lock.unlock();
        }
    }

    public static String getProperty(String key){
        String val = "";
        if(p != null && p.size() > 0) {
            val = p.getProperty(key);
        } else {
            String msg = "";
            if(p.size() == 0) {
                msg = "请确认是否load()过配置文件.";
            }
            LogUtils.getLogger().info("key:{}不存在。{}", key, msg);
        }

        return val;
    }
}