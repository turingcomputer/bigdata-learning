package com.turing.common.utils;

import org.apache.flink.api.common.restartstrategy.RestartStrategies;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.runtime.state.hashmap.HashMapStateBackend;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * @descri  FlinkEnvUtils
 *
 * @author lj.michale
 * @date 2022-12-29
 */
public class FlinkEnvUtils {

    /**
     * @descri 获取StreamExecutionEnvironment执行环境
     *
     * String propertiesFilePath = "/myjob.properties";
     * // 读取参数
     * ParameterTool parameter = ParameterTool.fromPropertiesFile(propertiesFilePath);
     */
    public static StreamExecutionEnvironment getStreamExecutionEnv(ParameterTool parameter) {
        StreamExecutionEnvironment streamEnv = StreamExecutionEnvironment.getExecutionEnvironment();
        streamEnv.setParallelism(parameter.getInt("flink.pipeline.parallelism"));
        streamEnv.enableCheckpointing(5 * 60000L, CheckpointingMode.EXACTLY_ONCE);
        streamEnv.getCheckpointConfig().setCheckpointTimeout(10 * 60000L);
        streamEnv.getCheckpointConfig().setMaxConcurrentCheckpoints(2);
        streamEnv.setRestartStrategy(RestartStrategies.fixedDelayRestart(3, 5000L));
        streamEnv.setStateBackend(new HashMapStateBackend());
        // hdfs://server115:9000/flink/ck
        streamEnv.getCheckpointConfig().setCheckpointStorage(parameter.get("flink.pipeline.checkpoint.url"));
        // System.setProperty("HADOOP_USER_NAME", "turing");
        // 解决Failed to set setXIncludeAware(true) for parser报错
        System.setProperty("javax.xml.parsers.DocumentBuilderFactory", "com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl");
        System.setProperty("javax.xml.parsers.SAXParserFactory", "com.sun.org.apache.xerces.internal.jaxp.SAXParserFactoryImpl");

        return streamEnv;
    }


}
