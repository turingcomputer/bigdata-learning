package com.turing.common.utils;
/**
 * @descri
 *
 * @author lj.michale
 * @date 2023-02-22
 */
public class StrUtils {

    /**
     * insert数据，ClickHouse 字段的整合。
     *
     * @param tableColums
     * @param tablename
     * @return
     */
    public static String clickhouseInsertValue(String[] tableColums, String tablename,String dataBaseName){
        StringBuffer sbCloums = new StringBuffer();
        StringBuffer sbValues = new StringBuffer();
        for (String s:tableColums) {
            sbCloums.append(s).append(",");
            sbValues.append("?").append(",");
        }
        String colums=sbCloums.toString().substring(0,sbCloums.toString().length()-1);
        String values=sbValues.toString().substring(0,sbValues.toString().length()-1);
        String insertSQL="insert into "+dataBaseName+"."+tablename+" ( "+colums+" ) values ( "+values+")";

        return insertSQL;
    }
}
