package com.turing.common.utils;

import com.fasterxml.jackson.databind.deser.std.StringDeserializer;
import com.turing.bean.DwdOrderBean;
import com.turing.bean.Jason;
import com.turing.flink.serialize.DwdOrderBeanJSONDeSerializer;
import com.turing.flink.serialize.MyKafkaDeserialization;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.connector.kafka.source.reader.deserializer.KafkaRecordDeserializationSchema;


import java.util.Arrays;

/**
 * @descri
 *
 * @author lj.michale
 * @date 2023-02-22
 */
public class KafkaUtils {

    public static KafkaSource<String> getKafkaConsumer(String kafkaUrl, String topic, String groupId) {
        KafkaSource<String> source = KafkaSource.<String>builder()
                .setBootstrapServers(kafkaUrl)
                .setTopics(topic)
                .setGroupId(groupId).setStartingOffsets(OffsetsInitializer.earliest())
                .setValueOnlyDeserializer(new SimpleStringSchema())
                .build();

        return source;
    }

    public static KafkaSource<Jason> getKafkaConsumerByJason(String kafkaUrl, String topic, String groupId) {
        KafkaSource<Jason> source = KafkaSource.<Jason>builder()
                .setBootstrapServers(kafkaUrl)
                .setTopics(topic)
                .setGroupId(groupId)
                .setStartingOffsets(OffsetsInitializer.earliest())
                .setDeserializer(KafkaRecordDeserializationSchema.of(new MyKafkaDeserialization(true, true)))
                .build();

        return source;
    }

    public static KafkaSource<DwdOrderBean> getKafkaConsumerByDwdOrderBean(String kafkaUrl, String topic, String groupId) {
        KafkaSource<DwdOrderBean> source = KafkaSource.<DwdOrderBean>builder()
                .setBootstrapServers(kafkaUrl)
                .setTopics(topic)
                .setGroupId(groupId)
                .setStartingOffsets(OffsetsInitializer.earliest())
                .setDeserializer(KafkaRecordDeserializationSchema.of(new DwdOrderBeanJSONDeSerializer(true, true)))
                .build();

        return source;
    }
}
