package com.turing.common.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.locks.ReentrantLock;

/**
 * @descri
 *
 * @author lj.michale
 * @date 2022-12-02
 */
public class LogUtils {

    private static final String utilClass = LogUtils.class.getName();
    private static Logger logger = null;
    private static ReentrantLock lock = new ReentrantLock();

    public static Logger getLogger() {

        StackTraceElement[] stacks = Thread.currentThread().getStackTrace();

        int depath = 0;
        if(stacks!=null&&stacks.length>0){
            for(int i=0;i<stacks.length;i++){
                if(utilClass.equals(stacks[i].getClassName())){
                    depath = i+1;
                    break;
                }
            }
        }

        String className = stacks[depath].getClassName();
        lock.lock();
        try {
            logger = LoggerFactory.getLogger(className);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }

        return logger;
    }
}