package com.turing.bean;

import java.io.Serializable;

/**
 * @descri
 *
 * @author lj.michale
 * @date 2023-02-22
 */
public class ProductBean implements Serializable {

    private String productId = "";
    private int status = 0;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

}
