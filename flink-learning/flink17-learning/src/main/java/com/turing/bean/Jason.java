package com.turing.bean;
/**
 * @descri
 *
 * @author lj.michale
 * @date 2022-12-13
 */
public class Jason {

    public String topic;

    public long timestamp;

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
