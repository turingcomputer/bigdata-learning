package com.turing.bean;

import lombok.*;

/**
 * @descri
 *
 * @author lj.michale
 * @date 2023-02-27
 */
@Setter
@Getter
public class Student {

    private int id;

    private String name;

    private int age;

    public Student(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }
}
