package com.turing.bean;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @descri
 *
 * @author lj.michale
 * @date 2023-02-22
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RunEnv {

    private String clickHouseHost;

    private String clickHousePort;

    private String clickHouseUser;

    private String clickHousePassword;

    public String getClickHouseHost() {
        return clickHouseHost;
    }

    public void setClickHouseHost(String clickHouseHost) {
        this.clickHouseHost = clickHouseHost;
    }

    public String getClickHousePort() {
        return clickHousePort;
    }

    public void setClickHousePort(String clickHousePort) {
        this.clickHousePort = clickHousePort;
    }

    public String getClickHouseUser() {
        return clickHouseUser;
    }

    public void setClickHouseUser(String clickHouseUser) {
        this.clickHouseUser = clickHouseUser;
    }

    public String getClickHousePassword() {
        return clickHousePassword;
    }

    public void setClickHousePassword(String clickHousePassword) {
        this.clickHousePassword = clickHousePassword;
    }
}
