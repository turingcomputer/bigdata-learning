package com.turing.common;

import com.turing.common.constant.RedisProperConstant;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.streaming.connectors.redis.common.config.FlinkJedisSentinelConfig;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @descri
 *
 * @author lj.michale
 * @date 2022-12-02
 */
public class FlinkUtils {

    public static FlinkJedisSentinelConfig getRedisSinkConfig(ParameterTool parameterTool) {
        String redisHosts = parameterTool.get(RedisProperConstant.REDIS_HOSTS);
        Set<String> hosts = new HashSet<String>(Arrays.asList(redisHosts.split(",")));

        FlinkJedisSentinelConfig redisProduceConfig = new FlinkJedisSentinelConfig.Builder()
                .setSentinels(hosts)
                .setMasterName(parameterTool.get(RedisProperConstant.REDIS_MASTER))
                .setPassword(parameterTool.get(RedisProperConstant.REDIS_PASSWORD))
                .setMaxIdle(parameterTool.getInt(RedisProperConstant.REDIS_POOL_MAXIDEL))
                .setMaxTotal(parameterTool.getInt(RedisProperConstant.REDIS_POOL_MAXTOTAL))
                .setConnectionTimeout(parameterTool.getInt(RedisProperConstant.REDIS_TIMEOUT)).build();

        return redisProduceConfig;
    }

}
