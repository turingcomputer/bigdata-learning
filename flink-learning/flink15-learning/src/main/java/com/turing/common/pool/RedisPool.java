package com.turing.common.pool;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.turing.common.PropertiesUtils;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisSentinelPool;
import redis.clients.jedis.JedisShardInfo;
import redis.clients.jedis.Protocol;
import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.ShardedJedisPool;
import redis.clients.util.Hashing;
import redis.clients.util.Sharded;

/**
 * @descri
 *
 * @author lj.michale
 * @date 2022-12-02
 */
public class RedisPool {

    private static JedisPoolConfig poolConfig = null;
    //jedis连接池
    private static JedisPool jedisPool = null;

    private static JedisSentinelPool sentinelpool = null;

    private static ShardedJedisPool shardPool = null;

    static{
        init();
    }

    public static void init(){
        PropertiesUtils.load("redis.properties");
        initPoolConfig();
        initJedisPool();
        initJedisSentinelPool();
        initShardedJedisPool();
    }

    /**
     * 获取配置
     * @return
     */
    private static JedisPoolConfig initPoolConfig(){
        if(poolConfig==null){
            String maxTotal = PropertiesUtils.getProperty("redis.poolconfig.maxTotal");
            String maxIdle = PropertiesUtils.getProperty("redis.poolconfig.maxIdle");
            String minIdle = PropertiesUtils.getProperty("redis.poolconfig.minIdle");
            String testOnBorrow = PropertiesUtils.getProperty("redis.poolconfig.testOnBorrow");
            String testOnReturn = PropertiesUtils.getProperty("redis.poolconfig.testOnReturn");

            poolConfig = new JedisPoolConfig();
            poolConfig.setMaxTotal(Integer.parseInt(maxTotal));
            poolConfig.setMaxIdle(Integer.parseInt(maxIdle));
            poolConfig.setMinIdle(Integer.parseInt(minIdle));
            poolConfig.setTestOnBorrow(Boolean.parseBoolean(testOnBorrow));
            poolConfig.setTestOnReturn(Boolean.parseBoolean(testOnReturn));
            //连接耗尽时，是否阻塞，false会抛出异常，true会阻塞直到超时，默认是true
            poolConfig.setBlockWhenExhausted(true);
        }

        return poolConfig;
    }

    /**
     * 单节点连接池
     */
    private static void initJedisPool(){
        //检查连接池开启状态
        if(!open("redis.jedispool.open")){
            if(jedisPool!=null){
                if(!jedisPool.isClosed()){
                    jedisPool.close();
                    jedisPool = null;
                }
            }
            return;
        }

        if(jedisPool==null){
            String hostAndPort = PropertiesUtils.getProperty("redis.jedispool.hostandport");
            String password = PropertiesUtils.getProperty("redis.jedispool.password");

            if(hostAndPort!=null&&hostAndPort.trim().length()>0){
                hostAndPort = hostAndPort.trim().replaceAll(";", "");
                String[] hostAndPortArr = getHostAndPortArr(hostAndPort);
                if(hostAndPortArr!=null&&hostAndPortArr.length==2){
                    jedisPool = new JedisPool(poolConfig, hostAndPortArr[0], Integer.parseInt(hostAndPortArr[1]), getTimeout(), password);
                }
            }else{
                jedisPool = new JedisPool(poolConfig, Protocol.DEFAULT_HOST, Protocol.DEFAULT_PORT, getTimeout(), password);
            }
        }

    }

    /**
     * 该连接池用于应对Redis的Sentinel的主从切换机制，
     * 能够正确在服务器宕机导致服务器切换时得到正确的服务器连接，
     * 当服务器采用该部署策略的时候推荐使用该连接池进行操作；
     */
    private static void initJedisSentinelPool() {
        //检查连接池开启状态
        if(!open("redis.sentinelpool.open")){
            if(sentinelpool!=null){
                if(!sentinelpool.isClosed()){
                    sentinelpool.close();
                    sentinelpool = null;
                }
            }
            return;
        }

        if(sentinelpool == null){
            String hostAndPorts = PropertiesUtils.getProperty("redis.sentinelpool.hostandports");
            String masterName = PropertiesUtils.getProperty("redis.sentinelpool.masterName");
            String password = PropertiesUtils.getProperty("redis.sentinelpool.password");

            //监听器列表
            Set<String> sentinels = new HashSet<String>();

            if(hostAndPorts!=null&&hostAndPorts.trim().length()>0){
                hostAndPorts = hostAndPorts.trim();
                String hostAndPortArr[] = hostAndPorts.split(";");
                if(hostAndPortArr!=null&&hostAndPortArr.length>0){
                    for(String hostAndPort :hostAndPortArr){
                        if(hostAndPort.trim().length()==0){
                            continue;
                        }
                        sentinels.add(hostAndPort);
                    }
                }

                if(sentinels.size()==0){
                    sentinels.add(new HostAndPort(Protocol.DEFAULT_HOST, Protocol.DEFAULT_SENTINEL_PORT).toString());
                }
            }

            sentinelpool = new JedisSentinelPool(masterName, sentinels, poolConfig, getTimeout(), password, Protocol.DEFAULT_DATABASE);
        }

    }

    /**
     * 初始化ShardedJedisPool连接池
     */
    private static void initShardedJedisPool() {
        //检查连接池开启状态
        if(!open("redis.shardpool.open")){
            if(shardPool!=null){
                if(!shardPool.isClosed()){
                    shardPool.close();
                    shardPool = null;
                }
            }
            return;
        }

        if (shardPool == null) {
            List<JedisShardInfo> serverlist = new LinkedList<JedisShardInfo>();

            String hostAndPorts = PropertiesUtils.getProperty("redis.shardpool.hostandports");
            String password = PropertiesUtils.getProperty("redis.shardpool.password");

            if(hostAndPorts!=null&&hostAndPorts.trim().length()>0){
                String hostAndPortsArr[] = hostAndPorts.trim().split(";");
                if(hostAndPortsArr!=null&&hostAndPortsArr.length>0){
                    for(String hostAndPort :hostAndPortsArr){
                        if(hostAndPort.length()==0){
                            continue;
                        }

                        String [] hostAndPortArr = getHostAndPortArr(hostAndPort);
                        if(hostAndPortArr!=null&&hostAndPortArr.length==2){
                            JedisShardInfo jedisShardInfo = new JedisShardInfo(hostAndPortArr[0], Integer.parseInt(hostAndPortArr[1]), getTimeout());
                            jedisShardInfo.setPassword(password);
                            serverlist.add(jedisShardInfo);
                        }
                    }
                }
            }

            if(serverlist.size()==0){
                JedisShardInfo jedisShardInfo = new JedisShardInfo(Protocol.DEFAULT_HOST, Protocol.DEFAULT_PORT, getTimeout());
                jedisShardInfo.setPassword(password);
                serverlist.add(jedisShardInfo);
            }

            shardPool = new ShardedJedisPool(poolConfig, serverlist, Hashing.MURMUR_HASH, Sharded.DEFAULT_KEY_TAG_PATTERN);
        }

    }

    /**
     * 将ip和端口号分隔出来
     * @param hostAndPort
     * @return
     */
    private static String[] getHostAndPortArr(String hostAndPort){
        if(hostAndPort==null||hostAndPort.trim().length()==0){
            return null;
        }

        if (!hostAndPort.contains(":")) {
            return null;
        }

        return hostAndPort.split(":");
    }

    /**
     * 获取超时设置
     * @return
     */
    private static int getTimeout(){
        int timeout = Protocol.DEFAULT_TIMEOUT;
        String timeoutStr = PropertiesUtils.getProperty("redis.timeout");
        if(timeoutStr!=null&&timeoutStr.trim().length()>0){
            timeout = Integer.parseInt(timeoutStr);
        }
        return timeout;
    }

    /**
     * 获取启动那种连接池设置
     * @param bundleAttrName
     * @return
     */
    private static boolean open(String bundleAttrName){
        boolean open = false;
        String openVal = PropertiesUtils.getProperty(bundleAttrName);
        if(openVal.contains("true")||openVal.contains("open")||openVal.contains("1")){
            open = true;
        }
        return open;
    }



    public static Jedis getJedis(){
        Jedis jedis = null;
        if(jedisPool!=null){
            jedis = jedisPool.getResource();
        }
        return jedis;
    }

    public static Jedis getSentinelJedis(){
        Jedis jedis = null;
        if(sentinelpool!=null){
            jedis = sentinelpool.getResource();
        }
        return jedis;
    }


    public static void returnJedis(Jedis jedis){
        if(jedis!=null){
            if(jedis.isConnected()){
                jedis.close();
            }
            jedis = null;
        }
    }


    public static ShardedJedis getShardedJedis(){
        ShardedJedis shardedJedis = null;
        if(shardPool!=null){
            shardedJedis = shardPool.getResource();
        }
        return shardedJedis;
    }

    public static void returnShardedJedis(ShardedJedis shardedJedis){
        if(shardedJedis!=null){
            shardedJedis.close();
            shardedJedis = null;
        }
    }

}