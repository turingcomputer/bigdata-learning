package com.turing.common;

import java.util.UUID;
import com.turing.common.pool.RedisPool;
import redis.clients.jedis.Jedis;

/**
 * @descri 封装jedis常用api
 *
 * @author lj.michale
 * @date 2022-12-02
 */
public class RedisUtils {

    /***
     * 根据key获取value
     * @param key
     * @return
     */
    public static String get(String key) {
        Jedis jedis = null;
        String value = null;
        try {
            jedis = RedisPool.getJedis();
            value = jedis.get(key);
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.getLogger().error("get error key{}", key, e);
            return null;
        } finally {
            RedisPool.returnJedis(jedis);
        }

        LogUtils.getLogger().info("get key:{} value:{}", key, value);

        return value;
    }

    /***
     * 删除key
     * @param key
     * @return
     */
    public static Long del(String key) {
        Jedis jedis = null;
        Long result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.del(key);
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.getLogger().error("del error key:{}", key, e);
            return null;
        } finally {
            RedisPool.returnJedis(jedis);
        }

        LogUtils.getLogger().info("del key:{} result:{}", key, result);

        return result;
    }

    /**
     * 根据key set value
     * @param key
     * @param value
     * @return
     */
    public static String set(String key,String value) {
        Jedis jedis = null;
        String result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.set(key,value);
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.getLogger().error("set error key:{} value:{}", key, value, e);
            return null;
        } finally {
            RedisPool.returnJedis(jedis);
        }

        LogUtils.getLogger().info("set key:{} value:{} result:{}", key, value, result);

        return result;
    }

    /***
     *设置session服务器有效时间
     * @param key
     * @param value
     * @param exTime 单位是秒
     * @return
     */
    public static String setEx(String key, String value, int exTime) {
        Jedis jedis = null;
        String result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.setex(key, exTime, value);
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.getLogger().error("setEx error key:{} value:{} exTime:{}", key, value, exTime, e);
            return null;
        } finally {
            RedisPool.returnJedis(jedis);
        }

        LogUtils.getLogger().info("setEx key:{} value:{} exTime:{} result:{}", key, value, exTime, result);

        return result;
    }

    /**
     * 设置key的有效期
     * @param key
     * @param exTime
     * @return
     */
    public static Long expire(String key, int exTime){
        Jedis jedis = null;
        Long result = null;
        try {
            jedis = RedisPool.getJedis();
            result = jedis.expire(key,exTime);
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.getLogger().error("expire error key:{} exTime:{}", key, exTime, e);
            return null;
        } finally {
            RedisPool.returnJedis(jedis);
        }

        LogUtils.getLogger().info("expire key:{} exTime:{} result:{}", key, exTime, result);

        return result;
    }

    public static String createKey() {
        long threadId = Thread.currentThread().getId();
        int threadHash = Thread.currentThread().hashCode();
        long currentTime = System.currentTimeMillis();
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        StringBuilder s = new StringBuilder();
        s.append(threadId);
        s.append("-");
        s.append(threadHash);
        s.append("-");
        s.append(currentTime);
        s.append("-");
        s.append(uuid);

        LogUtils.getLogger().info(s.toString());
        return s.toString();
    }

    public static void main(String[] args) {
        String key  = createKey();
        System.out.println(key);
    }
}