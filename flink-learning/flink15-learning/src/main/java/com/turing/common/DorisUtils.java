package com.turing.common;

import com.alibaba.fastjson2.JSONObject;
import org.apache.doris.flink.cfg.DorisExecutionOptions;
import org.apache.doris.flink.cfg.DorisOptions;
import org.apache.doris.flink.cfg.DorisReadOptions;
import org.apache.doris.flink.deserialization.DorisDeserializationSchema;
import org.apache.doris.flink.sink.DorisSink;
import org.apache.doris.flink.sink.writer.SimpleStringSerializer;
import org.apache.doris.flink.source.DorisSource;
import org.apache.doris.flink.source.DorisSourceBuilder;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.util.Collector;

import java.util.List;
import java.util.Properties;

/**
 * @descri Doris工具类
 *
 * @author lj.michale
 * @date 2022-12-19
 */
public class DorisUtils {

    /**
     * @descri
     *
     * @param fe_ip
     * @param tableName
     * @param userName
     * @param passWord
     * @param fields
     */
    public static DorisSource<JSONObject> getJsonDorisSource(String fe_ip,
                                                             String tableName,
                                                             String userName,
                                                             String passWord, String fields) {
        DorisSource<JSONObject> dorisSource = DorisSourceBuilder.<JSONObject>builder()
                .setDorisOptions(
                        DorisOptions.builder()
                                .setFenodes(fe_ip)
                                .setTableIdentifier(tableName)
                                .setUsername(userName)
                                .setPassword(passWord)
                                .build())
                .setDorisReadOptions(DorisReadOptions.builder().setReadFields(fields).build())
                .setDeserializer(new DorisDeserializationSchema<JSONObject>() {
                    @Override
                    public void deserialize(List<?> record, Collector<JSONObject> out) throws Exception {
                        int index = 0;
                        JSONObject result = new JSONObject();
                        for (String key : fields.split(",")) {
                            result.put(key, record.get(index));
                            index++;
                        }
                        out.collect(result);
                    }

                    @Override
                    public TypeInformation<JSONObject> getProducedType() {
                        return TypeInformation.of(new TypeHint<JSONObject>() {
                        });
                    }
                })
                .build();

        return dorisSource;
    }

    /**
     * @descri
     *
     * @param host
     * @param port
     * @param db
     * @param tb
     * @param user
     * @param pw
     * @param labelPrefix
     */
    public static DorisSink<String> getDorisSink(String host,
                                                 String port,
                                                 String db,
                                                 String tb,
                                                 String user,
                                                 String pw, String labelPrefix) {
        // doris-sink
        DorisSink.Builder<String> builder = DorisSink.builder();
        final DorisReadOptions.Builder readOptionBuilder = DorisReadOptions.builder();
        Properties pro = new Properties();
        pro.setProperty("format", "json");
        pro.setProperty("read_json_by_line", "true");
        pro.setProperty("line_delimiter", "\n");
        DorisOptions.Builder dorisBuilder = DorisOptions.builder();
        dorisBuilder.setFenodes(host + ":" + port)
                .setTableIdentifier(db + "." + tb)
                .setUsername(user)
                .setPassword(pw);

        DorisExecutionOptions.Builder executionBuilder = DorisExecutionOptions.builder();
        executionBuilder
                .setStreamLoadProp(pro)
                .setLabelPrefix(labelPrefix);

        DorisSink<String> dorisSink = builder.setDorisReadOptions(readOptionBuilder.build())
                .setDorisExecutionOptions(executionBuilder.build())
                .setSerializer(new SimpleStringSerializer())
                .setDorisOptions(dorisBuilder.build())
                .build();

        return dorisSink;
    }

}