package com.turing.common.constant;
/**
 * @descri
 *
 * @author lj.michale
 * @date 2022-12-02
 */
public class RedisProperConstant {

    public static String REDIS_HOSTS = "redis.jedispool.hostandport";

    public static String REDIS_MASTER = "redis.sentinelpool.masterName";

    public static String REDIS_PASSWORD = "";

    public static String REDIS_POOL_MAXIDEL = "";

    public static String REDIS_POOL_MAXTOTAL = "";

    public static String REDIS_TIMEOUT = "";

}
