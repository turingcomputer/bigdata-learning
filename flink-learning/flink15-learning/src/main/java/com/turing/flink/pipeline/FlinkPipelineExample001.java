package com.turing.flink.pipeline;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * @descri
 *
 * @author lj.michale
 * @date 2022-11-30
 */
public class FlinkPipelineExample001 {

    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        SingleOutputStreamOperator<Car> source = env.readTextFile("E:\\company\\Turing\\bigdata\\bigdata-learning\\docs\\cars.csv")
                .map((MapFunction<String, Car>) str -> {
                    System.out.println("##############" + str);
                    String[] props = str.split(",");
                    return new Car(props[0], props[1], Double.parseDouble(props[2]));
                });

        source.print();
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>");

        env.execute();
    }

    static class Car {
        // 品牌
        private String brand;

        // 型号
        private String model;

        // 价格
        private double price;

        public Car() {
        }

        public Car(String brand, String model, double price) {
            this.brand = brand;
            this.model = model;
            this.price = price;
        }
    }

}
