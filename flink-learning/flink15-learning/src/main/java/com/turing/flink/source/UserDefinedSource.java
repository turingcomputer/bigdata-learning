package com.turing.flink.source;

import com.turing.bean.Jason;
import org.apache.flink.streaming.api.functions.source.SourceFunction;

/**
 * @descri
 *
 * @author lj.michale
 * @date 2022-12-13
 */
public class UserDefinedSource implements SourceFunction<Jason> {

    @Override
    public void run(SourceContext<Jason> sourceContext) throws Exception {

    }

    @Override
    public void cancel() {

    }
}
