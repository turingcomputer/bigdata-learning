package com.turing.flink.sink.redis;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.connectors.redis.common.mapper.RedisCommand;
import org.apache.flink.streaming.connectors.redis.common.mapper.RedisCommandDescription;
import org.apache.flink.streaming.connectors.redis.common.mapper.RedisMapper;

/**
 * @descri Redis存储的key和value
 *
 * @author lj.michale
 * @date 2022-12-02
 */
public class SinkRedisMapper implements RedisMapper<Tuple2<String, String>> {

    /**
     * 设置使用的redis数据结构类型，和key的名词
     * 通过RedisCommand设置数据结构类型
     * Returns descriptor which defines data type.
     *
     * @return data type descriptor
     */
    @Override
    public RedisCommandDescription getCommandDescription() {
        // hset
        return new RedisCommandDescription(RedisCommand.HSET, "flink");
    }

    /**
     * 设置value中的键值对 key的值
     * Extracts key from data.
     *
     * @return key
     */
    @Override
    public String getKeyFromData(Tuple2<String, String> stringStringTuple2) {
        return stringStringTuple2.f0;
    }

    /**
     * 设置value中的键值对 value的值
     * Extracts value from data.
     *
     * @return value
     */
    @Override
    public String getValueFromData(Tuple2<String, String> stringStringTuple2) {
        return stringStringTuple2.f1.toString();
    }
}
