package com.turing.bean;
/**
 * @descri
 *
 * @author lj.michale
 * @date 2022-12-13
 */
public class Jason {

    public String name;

    public int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
