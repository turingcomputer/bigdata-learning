package com.turing.spark.function;

import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.PairFunction;
import scala.Tuple2;

/**
 * @descri 重写PairFunction处理函数接口
 * map 键值对 ，类似于MR的map方法
 * pairFunction<T,K,V>: T:输入类型；K,V：输出键值对
 * 表示输入类型为T,生成的key-value对中的key类型为k,value类型为v,对本例,T=String, K=String, V=String("")
 * 需要重写call方法实现转换
 *
 * @author lj.michale
 * @date 2022-12-02
 */
public class StrPairFunction implements PairFunction<String, String, String> {

    @Override
    public Tuple2<String, String> call(String s) throws Exception {
        return new Tuple2<String, String>(s, "");
    }

}
