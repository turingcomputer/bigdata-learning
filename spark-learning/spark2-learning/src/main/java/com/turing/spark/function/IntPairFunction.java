package com.turing.spark.function;

import org.apache.spark.api.java.function.PairFunction;
import scala.Tuple2;

/**
 * @descri
 *
 * @author lj.michale
 * @date 2022-12-02
 */
public class IntPairFunction implements PairFunction<String, String, Integer> {

    @Override
    public Tuple2<String, Integer> call(String s) throws Exception {
        return new Tuple2<String, Integer>(s, 1);
    }
}
