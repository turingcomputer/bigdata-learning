package com.turing.spark.pipeline;

import com.turing.spark.function.StrPairFunction;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import scala.Tuple2;

/**
 * @descri
 *
 * @author lj.michale
 * @date 2022-12-01
 */
public class SparkPipelineDeduplicateExample {

    public static void main(String[] args) {

        SparkConf conf = new SparkConf()
                .setAppName("SparkPipelineDeduplicateExample")
                .setMaster("local[2]");
        JavaSparkContext sc = new JavaSparkContext(conf);

        JavaRDD<String> dataRDD1 = sc.textFile("E:\\company\\Turing\\bigdata\\bigdata-learning\\docs\\file1.csv");
        JavaRDD<String> dataRDD2 = sc.textFile("E:\\company\\Turing\\bigdata\\bigdata-learning\\docs\\file2.csv");

        JavaRDD<String> dataRDD = dataRDD1.union(dataRDD2);

        JavaPairRDD<String, String> map = dataRDD.mapToPair(new StrPairFunction());

        JavaPairRDD<String, String> result = map.reduceByKey(
                new Function2<String, String, String>() {
                    @Override
                    public String call(String s, String s2) throws Exception {
                        return s2;
                    }
                }
        );

        Object[] array = result.collect().toArray();
        for (Object o : array) {
            Tuple2<String,String> tuple2 = (Tuple2<String,String>) o;
            System.out.println(tuple2._1);
        }

    }

}
