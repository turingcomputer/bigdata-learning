package com.bigdata.spark.hbase

import java.net.URL
import org.apache.hadoop.fs.Path
import org.apache.hadoop.hbase.client.ConnectionFactory
import org.apache.hadoop.hbase.{HBaseConfiguration, KeyValue, TableName}
import org.apache.hadoop.hbase.io.ImmutableBytesWritable
import org.apache.hadoop.hbase.mapreduce.{HFileOutputFormat2, LoadIncrementalHFiles}
import org.apache.hadoop.mapreduce.Job
import org.apache.spark.sql.{Row, SparkSession}
import org.apache.spark.sql.types.{StringType, StructField, StructType}
import scala.collection.mutable.ArrayBuffer

/**
 * @descri HBase bulkload写入
 *
 * @author lj.michale
 * @date 2022-12-13
 */
object SparkBulkloadExample001 {

  def main(args: Array[String]): Unit = {

    // local模式来执行spark;
    val session = SparkSession.builder().appName("test")
      .master("local[1]")
      .config("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
      .getOrCreate()

    val rdd = session.sparkContext.textFile("/home/hkws/zhens/abc.txt")
    val schema = StructType(List(StructField("id", StringType, true), StructField("name", StringType, true)))

    val rdd2 = rdd.map(e => {
      val element = e.split(",")
      println(s"${element(0)}------------${element(1)}")
      Row(element(0), element(1))
    })
    // 能转换成dataframe的rdd,必定是rdd[Row]
    val dataframe = session.createDataFrame(rdd2, schema)
    dataframe.show()
    // 必须调用sortByKey进行排序;
    // hbase每个列值都是一个keyvalue,对于dataframe中有很多列的数据,这么用flatmap较为合适;

    val rdd3 = rdd2.flatMap(row => {
      val result = new ArrayBuffer[Tuple2[ImmutableBytesWritable, KeyValue]]()
      val rowkey = row.getAs[String](0)
      val value = row.getAs[String](1)
      // KeyValue类接收的四个参数(rowkey、列簇、列名、值);在hbase里只有字节类型;
      val keyValue = new KeyValue(rowkey.getBytes, "cf".getBytes(), "name".getBytes, value.getBytes())
      result.+=((new ImmutableBytesWritable(rowkey.getBytes()), keyValue))
      result
    }).sortByKey()
    val conf = HBaseConfiguration.create();
    conf.addResource(new URL("file:///home/hkws/zhens/core-site.xml"));
    conf.addResource(new URL("file:///home/hkws/zhens/hdfs-site.xml"));
    conf.addResource(new URL("file:///home/hkws/zhens/hbase-site.xml"));
    // 只是生成了若干个hfile文件;后期加载到各个region时,仍需要进行拆分;
    rdd3.saveAsNewAPIHadoopFile(
      "/user/hkws/test",
      classOf[ImmutableBytesWritable],
      classOf[KeyValue],
      classOf[HFileOutputFormat2],
      conf
    )

    // 一个将hfile加载到hbase表的工具类;
    val loadIncrementalHFiles = new LoadIncrementalHFiles(conf)
    val connection = ConnectionFactory.createConnection(conf);
    val table = connection.getTable(TableName.valueOf("zte"))
    val regionLocator = connection.getRegionLocator(TableName.valueOf("zte"))
    val job = Job.getInstance(conf)
    job.setJobName("load")
    job.setMapOutputKeyClass(classOf[ImmutableBytesWritable])
    job.setMapOutputValueClass(classOf[KeyValue])
    HFileOutputFormat2.configureIncrementalLoad(job,table,regionLocator)
    loadIncrementalHFiles.doBulkLoad(new Path("/user/hkws/test"),connection.getAdmin,table,regionLocator);
    System.out.println("load success")

  }

}
