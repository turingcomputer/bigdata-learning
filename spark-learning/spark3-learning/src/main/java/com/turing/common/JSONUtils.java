package com.turing.common;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import org.apache.commons.lang3.StringUtils;

/**
 * @descri
 *
 * @author lj.michale
 * @date 2022-12-06
 */
public class JSONUtils {

    /**
     * Bean对象转JSON
     *
     * @param object
     * @param dataFormatString
     * @return
     */
    public static String beanToJson(Object object,
                                    String dataFormatString) {
        if (object != null) {
            if (StringUtils.isEmpty(dataFormatString)) {
                return JSONObject.toJSONString(object);
            }
            return JSON.toJSONString(object, dataFormatString);
        } else {
            return null;
        }
    }
    /**
     * Bean对象转JSON
     *
     * @param object
     * @return
     */
    public static String beanToJson(Object object) {
        if (object != null) {
            return JSON.toJSONString(object);
        } else {
            return null;
        }
    }

    /**
     * 将json字符串转换成对象
     *
     * @param json
     * @param clazz
     * @return
     */
    public static Object jsonStrToBean(String json,
                                    Object clazz) {
        if (StringUtils.isEmpty(json) || clazz == null) {
            return null;
        }

        return JSON.parseObject(json, clazz.getClass());
    }

}