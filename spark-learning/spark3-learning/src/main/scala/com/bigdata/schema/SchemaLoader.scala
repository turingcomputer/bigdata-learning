package com.bigdata.schema

import org.apache.spark.sql.types.{DataTypes, StructType}

/**
 * @descri 这个类中是封装了数据集的schema信息，主要用于读取数据源是指定schema信息
 *
 */
class SchemaLoader {

  /**
   * @descri movies数据集schema信息
   */
  private val movieSchema = new StructType()
    .add("movieId", DataTypes.StringType, false)
    .add("title", DataTypes.StringType, false)
    .add("genres", DataTypes.StringType, false)

  /**
   * @descri ratings数据集schema信息
   */
  private val ratingSchema = new StructType()
    .add("userId", DataTypes.StringType, false)
    .add("movieId", DataTypes.StringType, false)
    .add("rating", DataTypes.StringType, false)
    .add("timestamp", DataTypes.StringType, false)

  def getMovieSchema: StructType = movieSchema

  def getRatingSchema: StructType = ratingSchema

}
