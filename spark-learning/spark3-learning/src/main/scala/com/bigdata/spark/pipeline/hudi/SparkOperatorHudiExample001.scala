package com.bigdata.spark.pipeline.hudi

import org.apache.hudi.DataSourceWriteOptions
import org.apache.hudi.DataSourceWriteOptions._
import org.apache.hudi.config.HoodieWriteConfig
import org.apache.hudi.config.HoodieWriteConfig.TBL_NAME
import org.apache.hudi.hive.MultiPartKeysValueExtractor
import org.apache.hudi.keygen.ComplexKeyGenerator
import org.apache.spark.sql.SaveMode.{Append, Overwrite}
import org.apache.spark.sql.hudi.command.UuidKeyGenerator
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}

/**
 * @descri SparkOperatorHudiExample001
 *
 * @author lj.michale
 * @date 2022-12-13
 */
object SparkOperatorHudiExample001 {

  def main(args: Array[String]): Unit = {



  }

}
