package com.bigdata.spark.pipeline

import com.turing.common.JSONUtils
import org.apache.spark.SparkContext
import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}

/**
 * @descri Spark Structured Streaming Example001
 *
 * @author lj.michale
 * @date 2022-12-06
 */
object StructuredStreamingExample001 {

  def main(args: Array[String]): Unit = {

    val spark =SparkSession.builder()
      .master("local[*]")
      .appName("Covid19_Data_Process")
      .getOrCreate()
    val sc = spark.sparkContext
    sc.setLogLevel("WARN")

    val kafkaDF = spark
      .readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", "hadoop102:9092,hadoop103:9092,hadoop104:9092")
      .option("subscribe", "covid19") // 订阅主题
      .load()

    // 取出消息中的value，并转化成String类型
    // 开启隐式转换
    import spark.implicits._
    val jsonStrDS = kafkaDF.selectExpr("CAST(value AS STRING)").as[String]

    // 处理数据,将json格式的数据转换成实体类
//    val covidBeanDS:Dataset[CovidBean] = jsonStrDS.map( jsonStr => {
//      JSONUtils.jsonStrToBean(jsonStr,classOf[CovidBean])
//    })

  }

  case class CovidBean()

}
