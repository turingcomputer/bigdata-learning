package com.bigdata.spark.pipeline.examples

import org.apache.spark.SparkConf
import org.apache.spark.internal.Logging
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.catalyst.{FunctionIdentifier, TableIdentifier}
import org.apache.spark.sql.catalyst.expressions.Expression
import org.apache.spark.sql.catalyst.parser.ParserInterface
import org.apache.spark.sql.catalyst.plans.logical.LogicalPlan
import org.apache.spark.sql.catalyst.rules.Rule
import org.apache.spark.sql.execution.{SparkPlan, SparkStrategy}
import org.apache.spark.sql.types.{DataType, StructType}

case class MyResolutionRule(spark: SparkSession) extends Rule[LogicalPlan] with Logging {
  override def apply(plan: LogicalPlan): LogicalPlan = {
    logInfo("开始应用 MyResolutionRule 优化规则")
    println("开始应用 MyResolutionRule 优化规则")
    plan
  }
}

case class MyPostHocResolutionRule(spark: SparkSession) extends Rule[LogicalPlan] with Logging {
  override def apply(plan: LogicalPlan): LogicalPlan = {
    logInfo("开始应用 MyPostHocResolutionRule 优化规则")
    println("开始应用 MyPostHocResolutionRule 优化规则")
    plan
  }
}

case class MyOptimizerRule(spark: SparkSession) extends Rule[LogicalPlan] with Logging {
  override def apply(plan: LogicalPlan): LogicalPlan = {
    logInfo("开始应用 MyOptimizerRule 优化规则")
    println("开始应用 MyOptimizerRule 优化规则")
    plan
  }
}

case class MyCheckRule(spark: SparkSession) extends (LogicalPlan => Unit) with Logging {
  override def apply(plan: LogicalPlan): Unit = {
    logInfo("开始应用 MyCheckRule 优化规则")
    println("开始应用 MyCheckRule 优化规则")
  }
}

case class MySparkStrategy(spark: SparkSession) extends SparkStrategy with Logging {
  override def apply(plan: LogicalPlan): Seq[SparkPlan] = {
    logInfo("开始应用 MySparkStrategy 优化规则")
    println("开始应用 MySparkStrategy 优化规则")
    Seq.empty
  }
}

// 自定义injectParser
case class MyParser(spark: SparkSession, delegate: ParserInterface) extends ParserInterface with Logging {
  override def parsePlan(sqlText: String): LogicalPlan =
    delegate.parsePlan(sqlText)
  override def parseExpression(sqlText: String): Expression =
    delegate.parseExpression(sqlText)
  override def parseTableIdentifier(sqlText: String): TableIdentifier =
    delegate.parseTableIdentifier(sqlText)
  override def parseFunctionIdentifier(sqlText: String): FunctionIdentifier =
    delegate.parseFunctionIdentifier(sqlText)
  override def parseTableSchema(sqlText: String): StructType =
    delegate.parseTableSchema(sqlText)
  override def parseDataType(sqlText: String): DataType =
    delegate.parseDataType(sqlText)

  override def parseMultipartIdentifier(sqlText: String): Seq[String] = ???

  override def parseQuery(sqlText: String): LogicalPlan = ???

}
/**
 * @descr SparkSQL处理的各个阶段扩展自定义实现
 *
 * @date 2023-03-08
 */
object SparlExampleTest {

  def main(args: Array[String]): Unit = {

    // TODO 创建SparkSQL的运行环境
    val sparkConf = new SparkConf().setMaster("local[2]").setAppName("SparlExampleTest")
    val spark = SparkSession.builder().config(sparkConf)
      .withExtensions(e => e.injectResolutionRule(MyResolutionRule))
      .withExtensions(e => e.injectPostHocResolutionRule(MyPostHocResolutionRule))
      .withExtensions(e => e.injectCheckRule(MyCheckRule))
      .withExtensions(e => e.injectOptimizerRule(MyOptimizerRule))
      .withExtensions(e => e.injectPlannerStrategy(MySparkStrategy))
      .withExtensions(e => e.injectParser(MyParser))
      .getOrCreate()

    import spark.implicits._
    Seq((Array(1,2),"jf"), (Array(2),"ab"))
      .toDF("a","b")
      .createOrReplaceTempView("table_name1")
    spark.sql("select a, b from table_name1")
      .explain(true)

    spark.stop()

  }

}
