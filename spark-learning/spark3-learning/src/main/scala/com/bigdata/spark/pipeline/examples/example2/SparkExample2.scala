//package com.bigdata.spark.pipeline.examples.example2
//
//import org.apache.spark.sql.catalyst.parser.ParserInterface
//import org.apache.spark.sql.{SparkSession, SparkSessionExtensions}
//
///**
// * @descri
// * @author lj.michale
// * @date 2023-03-07
// */
//object SparkExample2 {
//
//  def main(args: Array[String]): Unit = {
//
//    type ParserBuilder = (SparkSession, ParserInterface) => ParserInterface
//    type ExtensionsBuilder = SparkSessionExtensions => Unit
//    val parserBuilder: ParserBuilder = (_, parser) => new StrictParser(parser)
//    val extBuilder: ExtensionsBuilder = { e => e.injectParser(parserBuilder)}
//
//    val spark = SparkSession
//      .builder()
//      .appName("Spark SQL basic example")
//      .config("spark.master", "local[*]")
//      .withExtensions(extBuilder)
//      .getOrCreate()
//    val df = spark.read.json("E:\\company\\Turing\\bigdata\\bigdata-learning\\docs\\input\\People.json")
//    df.createOrReplaceTempView("person")
//
//    println(spark.sql("select * from person limit 3").queryExecution.optimizedPlan.numberedTreeString)
//
//    spark.stop()
//
//  }
//
//}
