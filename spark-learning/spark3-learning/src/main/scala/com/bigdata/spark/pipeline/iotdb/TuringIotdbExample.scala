package com.bigdata.spark.pipeline.iotdb

import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession

object TuringIotdbExample {

  def main(args: Array[String]): Unit = {

    val conf = new SparkConf()
    conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
    conf.set("spark.sql.adaptive.enable", "true")

    val spark: SparkSession = SparkSession
      .builder()
      .config(conf)
      .master("local[*]")
      .appName("CtgDataIndicatorsPlatformPipeline")
      .getOrCreate()

    val df = spark.read
      .format("org.apache.iotdb.spark.db")
      .option("url","jdbc:iotdb://192.168.10.102:6667/")
      .option("sql","select * from root").load

    df.printSchema()
    df.show()

    //// iotdb写入
    // import narrow table
    val df2 = spark.createDataFrame(List(
      (1L, "root.test.d0",1, 1L, 1.0F, 1.0D, true, "hello"),
      (2L, "root.test.d0", 2, 2L, 2.0F, 2.0D, false, "world")))
    val dfWithColumn2 = df2.withColumnRenamed("_1", "Time")
      .withColumnRenamed("_2", "device_name")
      .withColumnRenamed("_3", "s0")
      .withColumnRenamed("_4", "s1")
      .withColumnRenamed("_5", "s2")
      .withColumnRenamed("_6", "s3")
      .withColumnRenamed("_7", "s4")
      .withColumnRenamed("_8", "s5")
    dfWithColumn2
      .write
      .format("org.apache.iotdb.spark.db")
      .option("url", "jdbc:iotdb://127.0.0.1:6667/")
      .save
    // import wide table
    val df3 = spark.createDataFrame(List(
      (1L, 1, 1L, 1.0F, 1.0D, true, "hello"),
      (2L, 2, 2L, 2.0F, 2.0D, false, "world")))
    val dfWithColumn3 = df3.withColumnRenamed("_1", "Time")
      .withColumnRenamed("_2", "root.test.d0.s0")
      .withColumnRenamed("_3", "root.test.d0.s1")
      .withColumnRenamed("_4", "root.test.d0.s2")
      .withColumnRenamed("_5", "root.test.d0.s3")
      .withColumnRenamed("_6", "root.test.d0.s4")
      .withColumnRenamed("_7", "root.test.d0.s5")
    dfWithColumn3.write.format("org.apache.iotdb.spark.db")
      .option("url", "jdbc:iotdb://127.0.0.1:6667/")
      .option("numPartition", "10")
      .save

    spark.stop()

  }

}
