package com.bigdata.spark.pipeline.hudi

import org.apache.spark.sql.catalyst.expressions.{ConcatWs, Literal}
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.{Column, DataFrame, SaveMode, SparkSession}
import org.apache.spark.sql.functions

/**
 * @descri 将数据写入hudi表
 *
 * @author lj.michale
 * @date 2023-01-10
 */
object SparkOperatorHudiCOW {

  def main(args: Array[String]): Unit = {

    val spark = SparkSession.builder().
        master("local[*]").
        appName("SparkOperatorHudiCOW").
        config("spark.serializer", "org.apache.spark.serializer.KryoSerializer").
        // 扩展Spark SQL，使Spark SQL支持Hudi
        config("spark.sql.extensions", "org.apache.spark.sql.hudi.HoodieSparkSessionExtension").
        // 支持Hive，本地测试时，注释掉
        enableHiveSupport().
        getOrCreate()

    import spark.implicits._
    val datasetFilePath: String = "file:///E:\\company\\Turing\\bigdata\\bigdata-learning\\docs\\input\\dwv_order_make_haikou_1.txt"

    val df: DataFrame = spark.read
      .option("sep", "\\t") // 设置分隔符为制表符
      .option("header", "true") // 文件首行为列名称
      .option("inferSchema", "true") // 依据数值自动推断数据类型
      .csv(datasetFilePath) // 指定文件路径

    //2、对数据进行ETL转换操作：指定ts和partition_path列
    val insertDF: DataFrame = df
//      .withColumn("partition_path", concat_ws("-", col("year"), col("month"), col("day"))) //将分区字段设置成yyyy-mm-dd格式
      .drop("year", "month", "day") //删除单独的年月日三列数据
//      .withColumn("ts", unix_timestamp(col("departure_time"), "yyyy-MM-dd HH:mm:ss")) //设置数据合并时所依据主键字段

    //3、将数据写入到Huid表，设置为COW模式
    val hudiTableName: String = "didi_haikou_cow"
    val hudiTablePath: String = "/datas/hudi-warehouse/didi_haikou_cow"

    // 导入包
    import org.apache.hudi.DataSourceWriteOptions._
    import org.apache.hudi.config.HoodieWriteConfig._

    // 保存数据
    insertDF.write
      .mode(SaveMode.Overwrite)
      .format("hudi")
      .option(TABLE_TYPE.key(), COW_TABLE_TYPE_OPT_VAL) //设置表写出模式，默认cow模式
      .option("hoodie.insert.shuffle.parallelism", "2")
      .option("hoodie.upsert.shuffle.parallelism", "2")
      // Hudi 表的属性值设置
      .option(RECORDKEY_FIELD.key(), "order_id")
      .option(PRECOMBINE_FIELD.key(), "ts")
      .option(PARTITIONPATH_FIELD.key(), "partition_path")
      .option(TBL_NAME.key(), hudiTableName)
      .save(hudiTablePath)

    spark.stop()

  }

}
