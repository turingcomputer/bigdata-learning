package com.bigdata.spark.pipeline.examples.example3

import org.apache.spark.SparkContext
import org.apache.spark.sql.SparkSession

import scala.collection.mutable.ArrayBuffer


object SparkPipelineExample3 {

  def main(args: Array[String]): Unit = {

    val spark = SparkSession
      .builder()
      .appName("SparkPipelineExample3")
      .config("spark.master", "local[*]")
      .getOrCreate()

    spark.sparkContext.setLogLevel("ERROR")
    import spark.implicits._

    val words = spark.sparkContext.parallelize(List("spa;rk", "fli;nk", "fli;nk", "hado;op", "kafk;a", "ka;fka", "zooke;eper", "red;is", "ja;va"))
      .flatMap(line=>{
        var list = new ArrayBuffer[String]()
        for (elem <- line.split(";")) {
          list = list :+ elem
          println(elem)
        }
        println(list)
        list.toIterator
      })
      .map((_, 1))
      .reduceByKey(_+_)
      .foreach(println(_))

    spark.stop()


  }

}
