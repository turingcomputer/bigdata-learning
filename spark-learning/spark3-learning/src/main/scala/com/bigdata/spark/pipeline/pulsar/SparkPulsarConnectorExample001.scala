package com.bigdata.spark.pipeline.pulsar

import org.apache.spark.sql.SparkSession

object SparkPulsarConnectorExample001 {

  def main(args: Array[String]): Unit = {

    val spark =SparkSession.builder()
      .master("local[*]")
      .appName("SparkPulsarConnectorExample001")
      .getOrCreate()
    val sc = spark.sparkContext
    sc.setLogLevel("WARN")

    // 根据一个或多个主题创建流处理 Source。
    val df = spark
      .readStream
      .format("pulsar")
      .option("service.url", "pulsar://localhost:6650")
      .option("admin.url", "http://localhost:8080")
      .option("topicsPattern", "topic.*") // Subscribe to a pattern
      // .option("topics", "topic1,topic2")    // Subscribe to multiple topics
      // .option("topic", "topic1"). //subscribe to a single topic
//      .option("startingOffsets", startingOffsets)
      .load()

    import spark.implicits._
    df.selectExpr("CAST(__key AS STRING)", "CAST(value AS STRING)")
      .as[(String, String)]

    // 构建批处理 Source
//    val df = spark
//      .read
//      .format("pulsar")
//      .option("service.url", "pulsar://localhost:6650")
//      .option("admin.url", "http://localhost:8080")
//      .option("topicsPattern", "topic.*")
//      .option("startingOffsets", "earliest")
//      .option("endingOffsets", "latest")
//      .load()
//    df.selectExpr("CAST(__key AS STRING)", "CAST(value AS STRING)")
//      .as[(String, String)]

    // 使用数据中本身的 topic 字段向多个主题进行持续 Sink
//    val ds = df
//      .selectExpr("topic", "CAST(__key AS STRING)", "CAST(value AS STRING)")
//      .writeStream
//      .format("pulsar")
//      .option("service.url", "pulsar://localhost:6650")
//      .start()

    // 将批处理结果写回 Pulsar
    df.selectExpr("CAST(__key AS STRING)", "CAST(value AS STRING)")
      .write
      .format("pulsar")
      .option("service.url", "pulsar://localhost:6650")
      .option("topic", "topic1")
      .save()



  }

}
