package com.turing.example.common.utils

import com.typesafe.scalalogging.Logger
import java.text.SimpleDateFormat
import java.util.{Calendar, Date}

import scala.collection.mutable.ListBuffer

/**
 * @descri 日期处理工具集合
 *
 * @author lj.michale
 * @date 2023-02-10
 */
object DateTimeUtils {

  private val logger:Logger = Logger(this.getClass)

  /**
   * @descri 判断指定日期是否指定日期之间  yyyy-MM-dd
   */
  def isBetweenDateInterval(iday:String,
                            benginDay:String,
                            endDay:String): Boolean = {
    var isTure = false
    if (benginDay <= iday && iday >= endDay) {
      isTure = true
    }

    isTure
  }

  /**
   * @descri String类型日期转Date类型
   *
   * @param strDateTime
   * @param dateFormat  yyyy-MM-dd HH:mm:ss
   */
  def strDateTimeTransforDate(strDateTime:String,
                              dateFormat: String): Date = {
    val dateTime: Date = new SimpleDateFormat(dateFormat).parse(strDateTime)

    dateTime
  }

  /**
   * @descri Long类型日期转String类型
   *
   * @param longDateTime
   * @param dateFormat  yyyy-MM-dd HH:mm:ss
   */
  def longDateTimeTransforStrDate(longDateTime:Long,
                                  dateFormat: String): String = {
    val dateTime: String = new SimpleDateFormat(dateFormat).format(longDateTime * 1000)

    dateTime
  }

  /**
   * @descri 时间字符类型转Long类型
   *
   * @param dateTime
   * @param dateFormat
   */
  def dateTimeTransforLongDate(dateTime:String,
                               dateFormat: String): Long = {
    val dateTime: Long = new SimpleDateFormat(dateFormat).parse("2017-12-18 00:01:56").getTime

    dateTime
  }

  /**
   * @descri 时间转换为时间戳
   *
   * @param dateTime
   * @param dateFormat  yyyy-MM-dd HH:mm:ss
   */
  def tranTimeToLong(dateTime:String, dateFormat: String) :Long = {
    val fm = new SimpleDateFormat(dateFormat)
    val dateTime2 = fm.parse(dateTime)
    val timestamp: Long = dateTime2.getTime()

    timestamp
  }

  /**
   * @descri 时间戳转化为时间
   *
   * @param timestamp   1502036122000
   * @param dateFormat  yyyy-MM-dd HH:mm:ss
   */
  def tranTimestampToDateTime(timestamp:String, dateFormat: String) :String = {
    val fm = new SimpleDateFormat(dateFormat)
    val dateTime = fm.format(new Date(timestamp.toLong))

    dateTime
  }

  /**
   * @descri 获取今天日期 (格式：yyyy-MM-dd)
   */
  def getNowDate():String = {
    val nowDate:Date = new Date()
    val dateFormat:SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd")
    val currentDay = dateFormat.format(nowDate)

    currentDay
  }

  /**
   * 获取两个日期之间的日期
   *
   * @param beginDate 开始日期
   * @param endDate   结束日期
   * @return 日期集合
   */
  def getBetweenDates(beginDate:String,
                      endDate:String) :List[String] = {
    val startData = new SimpleDateFormat("yyyy-MM-dd").parse(beginDate);
    val endData = new SimpleDateFormat("yyyy-MM-dd").parse(endDate);

    val dateFormat:SimpleDateFormat = new SimpleDateFormat("yyyyMMdd")
    var buffer = new ListBuffer[String]
    buffer += dateFormat.format(startData.getTime())
    val tempStart = Calendar.getInstance()
    tempStart.setTime(startData)
    tempStart.add(Calendar.DAY_OF_YEAR, 1)
    val tempEnd = Calendar.getInstance()
    tempEnd.setTime(endData)

    while (tempStart.before(tempEnd)) {
      // result.add(dateFormat.format(tempStart.getTime()))
      buffer += dateFormat.format(tempStart.getTime())
      tempStart.add(Calendar.DAY_OF_YEAR, 1)
    }

    buffer += dateFormat.format(endData.getTime())
    buffer.toList
  }


}
