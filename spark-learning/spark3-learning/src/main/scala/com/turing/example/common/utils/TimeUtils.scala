//package com.turing.example.common.utils
//
//import java.text.SimpleDateFormat
//import java.util.{Calendar, Date, Locale}
//import org.joda.time.DateTime
//
///**
// * @descri
// *
// * @author lj.michale
// * @date 2023-02-10
// */
//object TimeUtils {
//
//  /**
//   * 一个小时的毫秒值
//   */
//  final val ONE_HOUR_MILLISECONDS = 60 * 60 * 1000
//
//  /**
//   * yyyy-MM-dd HH:mm
//   */
//  final val MINUTE_DATE_FORMAT = "yyyy-MM-dd HH:mm"
//  /**
//   * 5分钟的毫秒值
//   */
//  final val FIVE_MINUTE_MILLISECONDS = 5 * 60 * 1000
//
//  /**
//   * yyyy-MM-dd HH:mm:ss
//   */
//  final val SECOND_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss"
//
//  final val SECOND_DATE_ZN_FORMAT = "yyyy年MM月dd日 HH:mm:ss"
//
//  /**
//   * yyyy-MM-dd
//   */
//  final val DAY_DATE_FORMAT_ONE = "yyyy-MM-dd"
//
//  /**
//   * yyyyMMdd
//   */
//  final val DAY_DATE_FORMAT_TWO = "yyyyMMdd"
//
//  /**
//   * yyyyMMddHHmm
//   */
//  final val dAY_DATE_FORMAT_THREE = "yyyyMMddHHmm"
//
//
//  def getYesterdayTime(): (Long, Long) = {
//    val calendar = Calendar.getInstance
//    calendar.setTime(new Date(System.currentTimeMillis))
//    calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), 0, 0, 0)
//    val beginTime = ((calendar.getTimeInMillis - Constant.DAY_TIME_SECOND) / 1000) * 1000
//    calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), 23, 59, 59)
//    val endTime = ((calendar.getTimeInMillis - Constant.DAY_TIME_SECOND) / 1000) * 1000
//    (beginTime, endTime)
//  }
//
//  /**
//   * 时间戳=>字符串
//   */
//  def convertTimeStamp2DateStr(timestamp: Long, pattern: String): String = {
//    new DateTime(timestamp).toString(pattern)
//  }
//
//  def getToday(): String = {
//    DateTime.now().toString(Constant.DAY_FORMAT_STR)
//  }
//
//  def getYesterday(): String = {
//    DateTime.now().minusDays(1).toString(Constant.DAY_FORMAT_STR)
//  }
//
//  def getTomorrow(): String = {
//    DateTime.now().plusDays(1).toString(Constant.DAY_FORMAT_STR)
//  }
//
//  def getPreDayByDate(date: Long, dayMin: Int): String = {
//    val dt = new DateTime(date)
//    dt.minusDays(dayMin).toString(Constant.DAY_FORMAT_STR)
//  }
//
//  def getPreHourByDate(dateStr: String, pattern: String, hourMin: Int): String = {
//    val dt = convertDateStr2Date(dateStr, pattern)
//    dt.minusHours(hourMin).toString(Constant.Second_FORMAT_STR)
//  }
//
//  def getGapTimeBetweenNowAndEndOfDay(): Int = {
//    val now = DateTime.now().getMillis
//    val dt = new DateTime(getTomorrow)
//    val end = dt.withTimeAtStartOfDay().getMillis - 1
//    (end - now).toInt / 1000
//  }
//
//  /**
//   * 时间字符串=>时间戳
//   */
//  def convertDateStr2TimeStamp(dateStr: String, pattern: String): Long = {
//    new SimpleDateFormat(pattern).parse(dateStr).getTime
//  }
//
//  /**
//   * 时间字符串=>日期
//   */
//  def convertDateStr2Date(dateStr: String, pattern: String): DateTime = {
//    new DateTime(new SimpleDateFormat(pattern).parse(dateStr))
//  }
//
//
//  /**
//   * 时间戳=>日期
//   */
//  def convertTimeStamp2Date(timestamp: Long): DateTime = {
//    new DateTime(timestamp)
//  }
//
//  /**
//   * 时间戳=>小时数
//   */
//  def convertTimeStamp2Hour(timestamp: Long): Long = {
//    new DateTime(timestamp).hourOfDay().getAsString().toLong
//  }
//
//
//  /**
//   * 时间戳=>分钟数
//   */
//  def convertTimeStamp2Minute(timestamp: Long): Long = {
//    new DateTime(timestamp).minuteOfHour().getAsString().toLong
//  }
//
//  /**
//   * 时间戳=>秒数
//   */
//  def convertTimeStamp2Sec(timestamp: Long): Long = {
//    new DateTime(timestamp).secondOfMinute().getAsString.toLong
//  }
//
//  /**
//   * 时间戳=>毫秒
//   */
//  def convertTimeStamp2mil(timestamp: Long): Long = {
//    new DateTime(timestamp).millisOfSecond().getAsString.toLong
//  }
//
//  /**
//   * 时间=>字符串
//   */
//  def Time2TimeStr(date: Date, pattern: String): String = {
//    new SimpleDateFormat(pattern).format(date)
//  }
//
//  def getCurrentTime: Long = {
//    val currentTime = System.currentTimeMillis()
//    (currentTime - (currentTime % FIVE_MINUTE_MILLISECONDS)) / 1000
//  }
//
//  /**
//   * 例如: 2 --> 02
//   */
//  def addZero(hourOrMin: String): String = {
//    if (hourOrMin.toInt <= 9)
//      "0" + hourOrMin
//    else
//      hourOrMin
//  }
//
//  /**
//   * 例如：02 --> 2
//   */
//  def delZero(hourOrMin: String): String = {
//    var res = hourOrMin
//    if (!hourOrMin.equals("0") && hourOrMin.startsWith("0"))
//      res = res.replaceAll("^0", "")
//    res
//  }
//
//  /**
//   * yyyy-MM-dd --> yyyyMMdd
//   */
//  def dateStrPatternOne2Two(time: String): String = {
//    TimeUtils.convertTimeStamp2DateStr(TimeUtils.convertDateStr2TimeStamp(time, TimeUtils
//      .DAY_DATE_FORMAT_ONE), TimeUtils.DAY_DATE_FORMAT_TWO)
//  }
//
//  /**
//   * 获取星期几
//   */
//  def dayOfWeek(dateStr: String): Int = {
//    val sdf = new SimpleDateFormat("yyyy-MM-dd")
//    val date = sdf.parse(dateStr)
//
//    //    val sdf2 = new SimpleDateFormat("EEEE")
//    //    sdf2.format(date)
//
//    val cal = Calendar.getInstance();
//    cal.setTime(date);
//    var w = cal.get(Calendar.DAY_OF_WEEK) - 1;
//
//    //星期天 默认为0
//    if (w <= 0)
//      w = 7
//    w
//  }
//
//  /**
//   * 判断是否是周末
//   */
//  def isRestday(date: String): Boolean = {
//    val dayNumOfWeek = dayOfWeek(date)
//    dayNumOfWeek == 6 || dayNumOfWeek == 7
//  }
//
//  /**
//   * 2019-03-04T09:08:21.288Z 转成 2019-03-04 09:08:27
//   * @param time
//   * @return
//   */
//  def getNormalTime(time: String): String = {
//    val sdf1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
//    val sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
//    val date = sdf1.parse(time)
//    val str = sdf2.format(date)
//    str
//  }
//
//
//  def getLocalTime(time: String): String = {
//    var result = ""
//    if(time.contains("Z")) {
//      result = time.replace("Z", "") + "+0800"
//    }
//    result
//  }
//
//  /**
//   *
//   * 2019-03-04T09:08:26.994Z 转成北京时间 kibana
//   * @param time
//   * @return
//   */
//  def getCNTime(time: String): String = {
//    val sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
//    val date = sdf.parse(time)
//    val df = new SimpleDateFormat ("EEE MMM dd HH:mm:ss Z yyyy", Locale.UK)
//    val date2 = df.parse(date.toString)
//    val res = sdf.format(date2)
//    res
//  }
//}
