package com.turing.example.app

import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession

/**
 * @descri Spark应用基类
 */
abstract class BaseSparkApp extends AppTrait {

  protected final val appName = getClass.getSimpleName

  protected final var spark: SparkSession = _

  /**
   * 启动应用
   */
  final def startApp(): Unit = {
    val benginTime = System.currentTimeMillis()
    println(" ===========> " + appName + " start ")

    onInit()

    createSession()

    onRun()

    onStop()

    onDestroyed()

    val endTime = System.currentTimeMillis()
    println("===========> " + appName + " end costTime=" + (endTime - benginTime) / 1000 + "s")
  }

  /**
   * 手动停止应用
   */
  final def stopApp(): Unit = {
    onStop()
  }

  /**
   * 创建 SparkSession
   */
  private def createSession(): Unit = {
    spark = SparkSession.builder()
      .config(getConf)
      .enableHiveSupport()
      .getOrCreate()
  }

  /**
   * Spark应用配置
   *
   * @return SparkConf
   */
  protected def getConf: SparkConf = {
    new SparkConf()
      .setAppName(appName)
      .set("spark.network.timeout", "300")
      .set("spark.shuffle.io.retryWait", "30s")
      .set("spark.shuffle.io.maxRetries", "12")
      .set("spark.locality.wait", "9s")
  }

  /**
   * 初始化，应用运行前
   */
  override protected def onInit(): Unit = {}

  /**
   * 应用运行
   */
  override protected def onRun(): Unit

  /**
   * 应用结束
   */
  override protected def onStop(): Unit = {
    if (spark != null) spark.stop()
  }

  /**
   * 应用销毁后调用
   */
  override protected def onDestroyed(): Unit = {}

}
