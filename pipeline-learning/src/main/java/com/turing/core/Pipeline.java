package com.turing.core;

/**
 * @descri
 *
 * @author lj.michale
 * @date 2023-03-07
 */
public interface Pipeline {

    /**
     * pipeline start
     */
    void pipelineBegin();

    /**
     * pipeline end
     */
    void pipelineEnd();

}
