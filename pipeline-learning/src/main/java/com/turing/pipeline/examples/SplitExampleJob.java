package com.turing.pipeline.examples;

import com.turing.bean.ClientLogSink;
import com.turing.bean.ClientLogSource;
import com.turing.common.utils.FlinkEnvUtils;
import org.apache.flink.api.common.functions.RichFlatMapFunction;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.runtime.state.hashmap.HashMapStateBackend;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.apache.flink.util.Collector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;

import org.apache.commons.lang3.RandomUtils;

/**
 * @descri
 *
 * @author lj.michale
 * @date 2023-03-07
 */
public class SplitExampleJob {

    private static final Logger logger = LoggerFactory.getLogger(FlinkPipeineExample1.class);

    public static void main(String[] args) throws Exception{

        final String fileName = "application-dev.properties";
        InputStream inputStream = FlinkPipeineExample1.class.getClassLoader().getResourceAsStream(fileName);
        ParameterTool parameterTool = ParameterTool.fromPropertiesFile(inputStream);
        int parallelisNum = Integer.valueOf(parameterTool.get("flink.pipeline.parallelism","1"));
        String checkpointPath = parameterTool.get("flink.pipeline.checkpoint.url");
        System.out.println(parallelisNum);
        logger.info("flink.pipeline.parallelism:{}", parallelisNum);

        FlinkEnvUtils.FlinkEnv flinkEnv = FlinkEnvUtils.getStreamTableEnv(args);
        flinkEnv.env().setParallelism(parallelisNum);
        flinkEnv.env().setStateBackend(new HashMapStateBackend());
        flinkEnv.env().getCheckpointConfig().setCheckpointStorage(checkpointPath);

        DataStreamSource<ClientLogSource> dataStreamSource = flinkEnv.env().addSource(new UserDefinedSource());
        dataStreamSource.print();
//        dataStreamSource.keyBy(ClientLogSource::getId)
//                .flatMap(new ClientLogSourceFlatMap())
//                .print();

        flinkEnv.env().execute("SplitExampleJob");

    }

    private static class UserDefinedSource implements SourceFunction<ClientLogSource> {

        private volatile boolean isCancel;

        @Override
        public void run(SourceContext<ClientLogSource> sourceContext) throws Exception {
            while (!this.isCancel) {
                sourceContext.collect(
                        ClientLogSource
                                .builder()
                                .id(RandomUtils.nextInt(0, 10))
                                .price(RandomUtils.nextInt(0, 100))
                                .timestamp(System.currentTimeMillis())
                                .date(new Date().toString())
                                .build()
                );
                Thread.sleep(1000L);
            }
        }

        @Override
        public void cancel() {
            this.isCancel = true;
        }
    }

//    public static class ClientLogSourceFlatMap extends RichFlatMapFunction<ClientLogSource, Tuple3<Long, Integer, Double>> {
//        private ValueState<Tuple2<Integer, Double>> sum;
//
//        @Override
//        public void open(Configuration parameters) {
//            ValueStateDescriptor<Tuple2<Integer, Double>> descriptor =
//                    new ValueStateDescriptor<>(
//                            "sum", // the state name
//                            TypeInformation.of(new TypeHint<Tuple2<Integer, Double>>() {}), // type information
//                            Tuple2.of(0, 0D)); // default value of the state, if nothing was set
//            sum = getRuntimeContext().getState(descriptor);
//        }
//
//        @Override
//        public void flatMap(ClientLogSource transaction, Collector<Tuple3<Long, Integer, Double>> collector) throws Exception {
//            Tuple2<Integer, Double> value = sum.value();
//            value.f0 = value.f0+1;
//            value.f1 = value.f1+transaction.getPrice();
//            sum.update(value);
//            collector.collect(new Tuple3<>(transaction.getId(), value.f0, value.f1));
//        }
//    }

}
