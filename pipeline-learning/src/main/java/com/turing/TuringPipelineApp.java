package com.turing;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @descri 基于flink1.16的pipeline开发模版
 *
 * @author lj.michale
 * @date 2023-03-06
 */
public class TuringPipelineApp {

    private static final Logger logger = LoggerFactory.getLogger(TuringPipelineApp.class);

    public static void main(String[] args) {
        logger.info(" pipeline compute starting ... ! ");


        System.out.println(" ################## ");

    }

}
