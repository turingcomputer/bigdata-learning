package com.turing;

import java.util.Scanner;

/**
 * @descri
 *
 * @author lj.michale
 * @date 2022-12-03
 */
public class Example001 {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] w = {2,2,3,1,5,2};//花费数组
        int[] v = {2,3,1,5,4,3};//价值数组
        //dp[i][j]表示对于当前的i种物品，有j元钱去买能获取的最大个数
        int[][]dp = new int[7][n+1];//6种物品，n元金额可消耗

        for(int i = 1;i < 7;i++)
        {
            for(int j = 1;j < n+1; j++)//消耗j元钱
            {
                //判断是否能买下第i个商品，钱不够就买不了
                if(j<w[i-1])
                {
                    dp[i][j]=dp[i-1][j];
                }
                else//否则取：买第i个商品与不买第i个商品之间的最大值
                {
                    dp[i][j]=Math.max(dp[i-1][j],dp[i-1][j-w[i-1]]+v[i-1]);
                }
            }
        }
        int res=dp[6][n];
        System.out.println(res);

    }

}
